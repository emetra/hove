unit MrD2009;

interface

uses
 SysUtils, Windows;

{$IFNDEF Unicode}

var
  FormatSettings: TFormatSettings;
type
  TCharSet = set of char;

function CharInSet( ch: char; const ASet: TCharSet ): boolean;

{$ENDIF}

implementation

{$IFNDEF Unicode}

function CharInSet( ch: char; const ASet: TCharSet ): boolean;
begin
  Result := ch in ASet;
end;

initialization
  GetLocaleFormatSettings( GetUserDefaultLCID, FormatSettings );

{$ENDIF}

end.
