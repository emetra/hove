unit FLINKres;

interface

uses
  FLINKgen,
  {$IFDEF XmlParser}
  MrXmlParser,
  {$ENDIF}
  Classes, SysUtils;

type
  {$IFDEF XmlParser}
  TFLINKresult = class(TMrXmlList)
  {$ELSE}
  TFLINKresult = class(TStringList)
  {$ENDIF}
  public
    Descending: boolean;
    procedure Open;
    procedure Close;
    procedure CompareTo( AOtherResult: TFLINKresult; ErrList: TStringList);
    function CountAbove( const AThreshold: integer): integer;
  end;

var
  WarnedList: TStringList;
  FLINKresult: TFLINKresult;
  FLINKerrors: TStringList;

implementation

function TFLINKresult.CountAbove( const AThreshold: integer): integer;
var
  n: integer;
begin
  n := 0;
  Result := 0;
  while n < Count do begin
    if GetIntPortion(Self[n], TAG_REL) >= AThreshold then inc(Result);
    inc(n);
  end;
end;

procedure TFLINKresult.Open;
begin
  Clear;
  Add( '<InteractionList>' );
end;

procedure TFLINKresult.Close;
begin
  Add( '</InteractionList>' );
end;

procedure TFLINKresult.CompareTo( AOtherResult: TFLINKresult; ErrList: TStringList);
const
  sstr = '%-8s - %-8s - %1s';
var
  n1, n2: integer;
  Name1, Name2,
    ATC1, ATC2, Rel: string;
  dummy: integer;
  Short1, Short2: TStringList;
  LocMissing: string;
  WebMissing: string;
begin

  LocMissing := '';
  WebMissing := '';
  ErrList.Clear;

  Short1 := TStringList.Create;
  Short1.Sorted := true;
  Short1.Duplicates := dupIgnore;
  Short2 := TStringList.Create;
  Short2.Sorted := true;
  Short2.Duplicates := dupIgnore;

  n1 := 0;
  while n1 < Self.Count do begin
    ATC1 := GetPortion(Self[n1], TAG_ATC1);
    ATC2 := GetPortion(Self[n1], TAG_ATC2);
    Rel := GetPortion(Self[n1], TAG_REL);
    Name1 := GetPortion(Self[n1], TAG_NAME1);
    Name2 := GetPortion(Self[n1], TAG_NAME2);
    if (ATC1 <> '') and (ATC2 <> '') then
      if CompareStr(ATC1, ATC2) < 0 then
        Short1.Add(Format(sstr, [ATC1, ATC2, Rel]) {+ ' ' + Name1+'+'+Name2})
      else
        Short1.Add(Format(sstr, [ATC2, ATC1, Rel]) {+ ' ' + Name2+'+'+Name1});
    inc(n1);
  end;

  n2 := 0;
  while n2 < AOtherResult.Count do begin
    ATC1 := GetPortion(AOtherResult[n2], TAG_ATC1);
    ATC2 := GetPortion(AOtherResult[n2], TAG_ATC2);
    Rel := GetPortion(AOtherResult[n2], TAG_REL);
    Name1 := GetPortion(AOtherResult[n2], TAG_NAME1);
    Name2 := GetPortion(AOtherResult[n2], TAG_NAME2);
    if (ATC1 <> '') and (ATC2 <> '') then
      if CompareStr(ATC1, ATC2) < 0 then
        Short2.Add(Format(sstr, [ATC1, ATC2, Rel]) { + ' ' + Name1 + '+'+Name2})
      else
        Short2.Add(Format(sstr, [ATC2, ATC1, Rel]) { + ' ' + Name2 + '+'+Name1});
    inc(n2);
  end;

  n2 := 0;
  while n2 < Short2.Count do begin
    if not Short1.Find(Short2[n2], dummy) then begin
      try
        WarnedList.Add(Short2[n2] + ' - NEW');
        LocMissing := LocMissing + Short2[n2] + #13#10;
      except on E: Exception do
      end;
    end;
    inc(n2);
  end;

  n1 := 0;
  while n1 < Short1.Count do begin
    if not Short2.Find(Short1[n1], dummy) then begin
      try
        WarnedList.Add(Short1[n1] + ' - DEL');
        WebMissing := WebMissing + Short1[n1] + #13#10;
      except on E: Exception do
      end;
    end;
    inc(n1);
  end;
  if WebMissing <> '' then begin
    ErrList.Add('Found locally, but not on web:');
    ErrList.Add(WebMissing);
  end;
  if LocMissing <> '' then begin
    ErrList.Add('Found on web, but not locally');
    ErrList.Add('Missing');
    ErrList.Add(LocMissing);
  end;
  Short1.Free;
  Short2.Free;
end;

initialization
  FLINKresult := TFLINKresult.Create;
  {$IFDEF XmlParser}
  FLINKresult.LocalOnly := false;
  {$ENDIF}
  FLINKerrors := TStringList.Create;
  WarnedList := TStringList.Create;
  WarnedList.Sorted := true;
  WarnedList.Duplicates := dupError;
finalization
  FLINKresult.Free;
  FLINKerrors.Free;
  WarnedList.Free;
end.
