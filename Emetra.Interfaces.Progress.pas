unit Emetra.Interfaces.Progress;

interface

type
  IStatus = interface
    ['{C3F25F6E-92B3-45B5-B26B-16F876EFEF2D}']
    function GetInfo: string;
    procedure Done;
    procedure SetInfo(const s: string);
    property Info: string read GetInfo write SetInfo;
  end;

  IProgress = interface( IStatus )
    ['{EF5C8790-D9A9-4374-9F24-9981A492A600}']
    procedure SetProgress(const APercent: double);
    procedure SetHeader(const s: string);
    property Header: string write SetHeader;
    property Percent: double write SetProgress;
  end;

implementation

end.
