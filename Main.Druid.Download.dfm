object frmDruidDownload: TfrmDruidDownload
  Left = 0
  Top = 0
  AlphaBlend = True
  AlphaBlendValue = 200
  BorderStyle = bsDialog
  Caption = 'DRUID Download'
  ClientHeight = 82
  ClientWidth = 340
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object lblInfo: TLabel
    AlignWithMargins = True
    Left = 16
    Top = 49
    Width = 308
    Height = 13
    Margins.Left = 16
    Margins.Top = 0
    Margins.Right = 16
    Margins.Bottom = 8
    Align = alTop
    Caption = 'Starter nedlasting...'
    ExplicitWidth = 98
  end
  object ProgressBar1: TProgressBar
    AlignWithMargins = True
    Left = 16
    Top = 16
    Width = 308
    Height = 25
    Margins.Left = 16
    Margins.Top = 16
    Margins.Right = 16
    Margins.Bottom = 8
    Align = alTop
    TabOrder = 0
  end
  object Timer1: TTimer
    Enabled = False
    Interval = 200
    Left = 384
  end
end
