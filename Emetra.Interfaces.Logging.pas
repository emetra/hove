unit Emetra.Interfaces.Logging;

interface

uses
  Classes, Controls, Graphics, StdCtrls, Windows, IniFiles;

type

  TLogEntryType = (ltDebug, ltInfo, ltMessage, ltWarning, ltError, ltCritical);
  TLogLevel = TLogEntryType;

  ISimpleLogger = interface( IInterface )
    ['{C006C5A5-3041-408C-8225-5E852931EDC1}']
    { Accessors }
    function Get_Threshold: TLogLevel;
    function Get_ThresholdForDialog: TLogLevel;
    procedure Set_Threshold(ALevel: TLogLevel);
    procedure Set_ThresholdForDialog(ALevel: TLogLevel);
    { Other members }
    function LogYesNo(const s: string; const ALevel: TLogEntryType = ltMessage; const ACancel: boolean = false): boolean;
    function StdFileName: string;
    procedure EnterMethod( AInstance: TObject; const AMethodName: string );
    procedure LeaveMethod( AInstance: TObject; const AMethodName: string );
    procedure AddStrings( const ATitle: string; AStrings: TStrings );
    procedure Event(const s: string; const AParams: array of const; const ALogLevel: TLogEntryType = ltInfo); overload;
    procedure Event(s: string; const ALogLevel: TLogEntryType = ltInfo); overload;
    procedure Reset;
    procedure SilentError( const s: string );
    procedure Success( const s: string );
    procedure SetFilename(const AFileName: string; const AAppend: boolean = false);
    procedure ShowMessage(const AMessage: string; const ALevel: TLogEntryType = ltMessage; const AMaxTimes: Integer = maxint);
    property Threshold: TLogLevel read Get_Threshold write Set_Threshold;
    property ThresholdForDialog: TLogLevel read Get_ThresholdForDialog write Set_ThresholdForDialog;
  end;

  ILogger = interface(ISimpleLogger)
    ['{C006C5A5-3041-408C-8225-5E852931ECD1}']
    { Accessors }
    function Get_AppPath: string;
    function Get_Lines: TStrings;
    procedure Set_Lines(const Value: TStrings);
    { Other members }
    function GetMarker(const ALevel: TLogEntryType): string;
    function SetPriority(const APriorityClass: dword): boolean;
    function StripNewlines(const AMsg: string): string;
    procedure CustomDraw(Control: TWinControl; Index: Integer; Rect: TRect; State: TOwnerDrawState);
    procedure CustomDrawColors(ACanvas: TCanvas; const AIndex: Integer);
    procedure LogString(const AName, AValue: string);
    procedure SaveFatalExit;
    procedure SetUserFile(AFile: TIniFile);
    property AppPath: string read Get_AppPath;
    property Lines: TStrings read Get_Lines write Set_Lines;
  end;
  
const
  ltTrivialInfo = ltDebug;
  ltException = ltError;

implementation

end.
