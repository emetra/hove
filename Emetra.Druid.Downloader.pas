unit Emetra.Druid.Downloader;

interface

uses
  Classes, SysUtils,
  XmlIntf, XmlDoc, XmlDom;

type
  TOnLoad = procedure( ADocument: IXmlDocument; const AUrl: string ) of object;
  IStatusInfo = interface['{7AD55D71-14A9-4AAF-81A1-6371096748C7}']
    procedure SetInfo( const s: string );
    procedure SetProgress( const APercent: integer );
  end;

  TNodeHandler = procedure( ANode: IDOMNode ) of object;

  TDruidDownloader = class( TObject )
  private
    FTextFile: TStringList;
    FStatusInfo: IStatusInfo;
    FMax: integer;
    FMin: integer;
    FOnLoad: TOnLoad;
    FPath: string;
    function StripLinefeeds( const s: string ): string;
    procedure AddSubstance( ANode: IDOMNode );
    procedure AddInteraction( ANode: IDOMNode );
    procedure AddMetaMember( ANode: IDOMNode );
    procedure DownloadFile( const AWebPage, ANodeTagName, ALocalFileName: string; ANodeHandler: TNodeHandler );
    procedure SetProgressRange( const AStart, AStop: integer );
  public
    constructor Create;
    destructor Destroy; override;
    procedure Execute( AStatusInfo: IStatusInfo );
    property OnLoad: TOnLoad read FOnLoad write FOnLoad;
    property Path: string read FPath write FPath;
  end;

implementation

{ TDruidDownloader }

const
  COLUMN_SEPARATOR = ' '#9' ';
  WWW_SERVER = 'http://druid.emetra.no/';

constructor TDruidDownloader.Create;
begin
  inherited;
  FTextFile := TStringList.Create;
end;

destructor TDruidDownloader.Destroy;
begin
  FTextFile.Free;
  inherited;
end;

procedure TDruidDownloader.AddInteraction( ANode: IDOMNode);
begin
  with ANode.Attributes do
    FTextFile.Add( GetNamedItem('ATC1').nodeValue + COLUMN_SEPARATOR +  GetNamedItem('ATC2').nodeValue + COLUMN_SEPARATOR +
      GetNamedItem('Level').nodeValue + COLUMN_SEPARATOR + StripLineFeeds( GetNamedItem('Text').nodeValue ) );
end;

procedure TDruidDownloader.AddSubstance( ANode: IDOMNode);
begin
  with ANode.Attributes do
    FTextFile.Add( GetNamedItem('ATC').nodeValue + COLUMN_SEPARATOR +  Trim( GetNamedItem('name').nodeValue ) + COLUMN_SEPARATOR +
      GetNamedItem('src').nodeValue + COLUMN_SEPARATOR + GetNamedItem('maintained').nodeValue );
end;

procedure TDruidDownloader.AddMetaMember(ANode: IDOMNode);
var
  n: integer;
begin
  n := 0;
  while n < ANode.childNodes.length do
  begin
    FTextFile.Add( ANode.childNodes[n].attributes.getNamedItem('ATC').nodeValue + COLUMN_SEPARATOR + ANode.attributes.getNamedItem('ATC').nodeValue );
    inc( n );
  end;
end;

procedure TDruidDownloader.Execute( AStatusInfo: IStatusInfo );
begin
  FStatusInfo := AStatusInfo;
  SetProgressRange( 1, 2 );
  Assert( Assigned( AStatusInfo ), 'StatusInfo interface m� v�re satt' );
  SetProgressRange( 2, 30 );
  DownloadFile( 'interaction_xmllist.asp', 'Interaction', 'Interactions.tsv', AddInteraction );
  SetProgressRange( 30, 40 );
  DownloadFile( 'metagroup_xmllist.asp', 'metagroup', 'MetaMembers.tsv', AddMetaMember );
  SetProgressRange( 40, 50 );
  DownloadFile( 'classification_xmllist.asp', 'word', 'Classification.tsv', AddSubstance );
  SetProgressRange( 50, 100 );
  DownloadFile( 'allnames_xmllist.asp', 'word', 'Substances.tsv', AddSubstance );
  SetProgressRange( 100, 100 );
  FStatusInfo.SetInfo( 'Ferdig' );
end;

procedure TDruidDownloader.SetProgressRange(const AStart, AStop: integer);
begin
  FMin := AStart;
  FMax := AStop;
end;

function TDruidDownloader.StripLinefeeds(const s: string): string;
begin
  Result := StringReplace( s, #13, ' ', [rfReplaceAll] );
  Result := StringReplace( Result, #13, ' ', [rfReplaceAll] );
end;

procedure TDruidDownloader.DownloadFile( const AWebPage, ANodeTagName, ALocalFileName: string; ANodeHandler: TNodeHandler );
var
  n: integer;
  pct: double;
  xmlDocument: IXmlDocument;
  nodeList: IDomNodeList;
begin
  FTextFile.Clear;
  xmlDocument := NewXmlDocument;
  try
    FStatusInfo.SetInfo( Format( 'Laster ned %s', [AWebPage] ) );
    if Assigned( FOnLoad ) then
      FOnLoad( xmlDocument, WWW_SERVER + AWebPage )
    else
      xmlDocument.LoadFromFile( WWW_SERVER + AWebPage );
    FStatusInfo.SetInfo( Format( 'Analyserer %s', [AWebPage] ) );
    nodeList := xmlDocument.DOMDocument.getElementsByTagName( ANodeTagName );
    n := 0;
    while n < nodeList.Length do
    begin
      pct := n / nodeList.length;
      FStatusInfo.SetProgress( round( FMin + pct * (FMax-FMin) ) );
      ANodeHandler( nodeList[n] );
      inc( n );
    end;
    FStatusInfo.SetInfo( Format( 'Lagrer %s', [ALocalFileName] ) );
    FTextFile.SaveToFile( FPath + ALocalFileName);
  finally
    xmlDocument := nil;
  end;
end;

end.
