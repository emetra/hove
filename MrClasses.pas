unit MrClasses;

interface

uses
  Classes, Contnrs, Controls, Forms;

type
  TCodedText = class( TInterfacedPersistent )
  protected
    FCode: integer;
    FText: string;
    function Instance: TObject;
    function CodeText: string;
  public
    constructor Create( const ACode: integer; const AText: string );
    function AsListBox( const ASimple: boolean = true ): string;
    function AsString: string; dynamic;
    procedure Assign( ASource: TPersistent ); override;
    property Code: integer read FCode;
    property Text: string read FText;
  end;

  TTokenizer = class(TObject)
  private
    FList: TStringList;
    function Get_Item(AIndex: Integer): string;
  public
    constructor Create;
    destructor Destroy; override;
    function Count: Integer;
    function DelimitedText: string;
    function GetAt(const s: string; const n: Integer; const ASep: char): string;
    function IndexOf(const s: string): Integer;
    function Text: string;
    procedure Delete(const AIndex: Integer);
    procedure Prepare(const s: string; const ASep: char); overload;
    procedure Prepare(const s: string; const ASep: string); overload;
    property Items[AIndex: Integer]: string read Get_Item; default;
  end;

  { Helper to let TStrings free objects attached to it.  Use with care. }
  TStringObjectClear = class helper for TStrings
  public
    procedure ClearObjects;
  end;

  TMrInterfacedPersistent = class( TInterfacedPersistent )
  public
    function Instance: TObject;
  end;

  TMrComponent = class( TComponent )
  private
    class var InstanceCounter: integer;
  public
    constructor Create( AOwner: TComponent ); override;
    function Instance: TObject;
  end;

  TMrList = class( TMrInterfacedPersistent )
  private
  protected
    FList: TObjectList;
    function Get_Count: integer;
    function Get_Item(AIndex: Integer): TObject;
  public
    constructor Create; dynamic;
    destructor Destroy; override;
    procedure Clear;
    property Count: integer read Get_Count;
    property Items[AIndex:Integer]: TObject read Get_Item; default;
  end;

  TMrForm = class( TForm )
    function Instance: TObject;
  end;

var
  Tokens: TTokenizer;

implementation

uses
  MrBase, SysUtils;

procedure TStringObjectClear.ClearObjects;
var
  tmp: TObject;
begin
  BeginUpdate;
  try
    while Count > 0 do begin
      tmp := Objects[0];
      Delete(0);
      tmp.Free;
    end;
  finally
    EndUpdate;
  end;
end;

{$REGION 'TCodeValuePair'}

function TCodedText.CodeText: string;
begin
  Result := Format( '%d', [FCode] );
end;

constructor TCodedText.Create(const ACode: integer; const AText: string);
begin
  FCode := ACode;
  FText := AText;
end;

function TCodedText.Instance: TObject;
begin
  Result := Self;
end;

function TCodedText.AsListBox(const ASimple: boolean = true ): string;
begin
  Result := Format( '%d'#9'%s', [FCode, FText] );
end;

procedure TCodedText.Assign(ASource: TPersistent);
begin
  if ASource is TCodedText then
  begin
    FCode := TCodedText( ASource ).Code;
    FText := TCodedText( ASource ).Text;
  end;
end;

function TCodedText.AsString: string;
begin
  Result := Format( '%d %s', [FCode, FText] );
end;

{$ENDREGION}

{$REGION 'Tokenizer'}

function TTokenizer.GetAt(const s: string; const n: Integer; const ASep: char): string;
begin
  FList.Delimiter := ASep;
  FList.DelimitedText := s;
  if (n > 0) and (n < FList.Count + 1) then
    Result := FList[n - 1]
  else
    Result := EmptyStr;
end;

function TTokenizer.Count: Integer;
begin
  Result := FList.Count;
end;

constructor TTokenizer.Create;
begin
  FList := TStringList.Create;
  FList.StrictDelimiter := true;
end;

procedure TTokenizer.Delete(const AIndex: Integer);
begin
  FList.Delete(AIndex);
end;

function TTokenizer.DelimitedText: string;
begin
  Result := FList.DelimitedText;
end;

destructor TTokenizer.Destroy;
begin
  FList.Free;
  inherited;
end;

function TTokenizer.Get_Item(AIndex: Integer): string;
begin
  if (AIndex > -1) and (AIndex < FList.Count) then
    Result := Trim(FList[AIndex])
  else
    Result := EmptyStr;
end;

function TTokenizer.IndexOf(const s: string): Integer;
begin
  Result := FList.IndexOf(s);
end;

procedure TTokenizer.Prepare(const s: string; const ASep: char);
begin
  FList.Delimiter := ASep;
  FList.DelimitedText := s;
end;

procedure TTokenizer.Prepare(const s, ASep: string);
var
  delimChar: char;
begin
  { Find a single char delimiter instead of the separator string.
    The character can not be in the string already! }
  delimChar := #255;
  while Pos( delimChar, s) > 0 do
    Dec(delimChar);
  Assert( delimChar > #0 );
 { Call the standard prepare with this character }
 Prepare( StringReplace( s, ASep, delimChar, [rfReplaceAll, rfIgnoreCase]), delimChar );
end;

function TTokenizer.Text: string;
begin
  Result := FList.Text;
end;

{$ENDREGION}

function TMrInterfacedPersistent.Instance: TObject;
begin
  Result := Self;
end;
{ TMrComponent }

constructor TMrComponent.Create(AOwner: TComponent);
begin
  inherited;
  inc( InstanceCounter );
  Name := Format( '%s%d', [ClassName,InstanceCounter] );
end;

function TMrComponent.Instance: TObject;
begin
  Result := Self;
end;

{ TMrForm }

function TMrForm.Instance: TObject;
begin
  Result := Self;
end;

{ TMrList }

procedure TMrList.Clear;
begin
  FList.Clear;
end;

constructor TMrList.Create;
begin
  FList := TObjectList.Create( true );
end;

destructor TMrList.Destroy;
begin
  FList.Free;
  inherited;
end;

function TMrList.Get_Count: integer;
begin
  Result := FList.Count;
end;

function TMrList.Get_Item(AIndex: Integer): TObject;
begin
  Result := FList[AIndex];
end;

initialization
  Tokens := TTokenizer.Create;
finalization
  Tokens.Free;
end.
