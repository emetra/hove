unit DrugChecker;

{$DEFINE ProdVersion}

interface

uses
  {$IFNDEF NewVersion}
  FLINKlocal, FLINKgen,
  {$ENDIF}
  Classes, SysUtils, Contnrs;

type

  TKBDrug = class(TObject)
  private
  var
    FATC: String;
    FName: String;
  public
    property ATC: String read FATC;
    property Name: String read FName;
  end;

  TDrug = class(TKBDrug)
  private
    FNewToday: boolean;
  public
    constructor Create( const AATC, AName: string; const ANewToday: boolean = false );
    property NewToday: boolean read FNewToday;
  end;

  TDrugList = class( TObjectList )
  private
    function GetDrug(AIndex: integer): TDrug;
  public
    function AddDrug( const AATC, AName: string; const ANew: boolean = false ): TDrug;
    function Names: string;
    property Drug[AIndex: integer]: TDrug read GetDrug;
  end;

  { TKBInteraction can be directly mapped to downloaded XML from DRUID
    http://www.interaksjoner.no/interaction_xmllist.asp }

  TKBInteraction = class( TObject )
  private
    FIntId: integer;
    FAtc1: string;
    FAtc2: string;
    FLevelId: integer;
    FText: string;
  public
    constructor Create( const IntId: integer; const AATC1,AATC2: string; const ALevelId: integer;
      const AText: string );
    function SameAs( AOther: TKbInteraction ): boolean;
    procedure Clear;
    property IntId: integer read FIntId;
    property Text: string read FText;
    property LevelId: integer read FLevelId;
    property ATC1: string read FATC1;
    property ATC2: string read FATC2;
  end;

  { TKBInteraction holds the list of interaction as downloaded from www.interaksjoner.no }
  TKBInteractionList = class( TObjectList )
  private
    procedure AddRow( const AIntId: integer; const AATC1,AATC2: string; const ALevelId: integer;
      const AText: string );
  public
    procedure LoadFromFile( const AFileName: string );
  end;

  { TDrugInteraction adds two lists to identify the drugs that interacted.
    This class is used for drug interactions occuring in a particular patient }
  TDrugInteraction = class( TKBInteraction )
  private
    FDrugs1: TDrugList;
    FDrugs2: TDrugList;
    FOld: boolean;
  public
    procedure Clear;
    procedure AfterConstruction; override;
    procedure BeforeDestruction; override;
    property Old: boolean read FOld;
    property Drugs1: TDrugList read FDrugs1;
    property Drugs2: TDrugList read FDrugs2;
  end;

  TDrugChecker = class( TObject )
  private
    {$IFDEF NewVersion}
    FKbInteractions: TKBInteractionList;
    {$ELSE}
    FEM: TFlinkLocal;
    {$ENDIF}
    FActualInteractions: TObjectList;
    FDrugList: TDrugList;
    FReady: boolean;
    procedure AddResult( const AATC1, AATC2, Name1, Name2: string; const ALevelId: integer;  const AText: string );
  public
    procedure AfterConstruction; override;
    procedure BeforeDestruction; override;
    { User methods }
    function HasInteractions( out AOldCount, ANewCount: integer ): boolean;
    procedure AddDrug( const AATC, ADrugName: string; const ANew: boolean = false ); {
      Add a drug to check for interactions.  Interactions are not actually checked
      until HasInteractions is called. }
    function LoadKnowledgeData( const ALocation: string ): boolean; { Loads drug interaction knowledge from the database }
    procedure Clear; { Clears drugs added with AddDrug, and ActualInteractions }
    property ActualInteractions: TObjectList read FActualInteractions;
  end;

implementation

{ TDrug }
constructor TDrug.Create(const AATC, AName: string; const ANewToday: boolean = false );
begin
  FATC := AATC;
  FName := AName;
  FNewToday := ANewToday;
end;

{ TDrugList }

function TDrugList.AddDrug(const AATC, AName: string; const ANew: boolean = false ): TDrug;
begin
  Result := TDrug.Create( AATC, AName, ANew );
  Add( Result );
end;

function TDrugList.GetDrug(AIndex: integer): TDrug;
begin
  Result := TDrug( Self.List[AIndex] );
end;

function TDrugList.Names: string;
var
  n: integer;
  lstNames: TStringList;
begin
  lstNames := TStringList.Create;
  lstNames.Duplicates := dupIgnore;
  lstNames.Sorted := true;
  try
    n := 0;
    while n < Count do begin
      lstNames.Add( Self.Drug[n].Name );
      inc( n );
    end;
    Result := lstNames.CommaText;
  finally
    lstNames.Free;
  end;
end;

{ TBaseInteraction }

procedure TKBInteraction.Clear;
begin
  FIntId := 0;
  FATC1 := '';
  FATC2 := '';
  FLevelId := 0;
  FText := '';
end;

constructor TKBInteraction.Create(const IntId: integer; const AATC1, AATC2: string;
  const ALevelId: integer; const AText: string);
begin
  FIntId := IntId;
  FATC1 := AnsiUppercase( StringReplace( AATC1, ' ', '', [rfReplaceAll] ) );
  FATC2 := AnsiUppercase( StringReplace( AATC2, ' ', '', [rfReplaceAll] ) );
  FLevelId := ALevelId;
  FText := Trim( AText );
  Assert( Length( FATC1 )<= 7 );
  Assert( Length( FATC2 )<= 8 );
  Assert( ( FLevelId <= 4 ) and ( FLevelId >=0 ) );
  Assert( Length(FText) > 6 );
end;

function TKBInteraction.SameAs(AOther: TKbInteraction): boolean;
begin
  Result := SameText( FATC1, AOther.ATC1 ) and SameText( FATC2, AOther.ATC2 );
end;

{ TDrugInteraction }

procedure TDrugInteraction.AfterConstruction;
begin
  inherited;
  FDrugs1 := TDrugList.Create;
  FDrugs2 := TDrugList.Create;
end;

procedure TDrugInteraction.BeforeDestruction;
begin
  FDrugs1.Free;
  FDrugs2.Free;
  inherited;
end;

procedure TDrugInteraction.Clear;
begin
  inherited Clear;
  FDrugs1.Clear;
  FDrugs2.Clear;
end;

{ TDrugChecker }

procedure TDrugChecker.AfterConstruction;
begin
  inherited;
  {$IFNDEF NewVersion}
  FEM := TFlinkLocal.Create;
  FEM.OnInteraction := Self.AddResult;
  {$ENDIF}
  FDrugList := TDrugList.Create;
  FActualInteractions := TObjectList.Create;
end;

procedure TDrugChecker.BeforeDestruction;
begin
  inherited;
  {$IFNDEF NewVersion}
  FEM.Free;
  {$ENDIF}
  FDrugList.Free;
  FActualInteractions.Free;
end;

procedure TDrugChecker.AddDrug(const AATC, ADrugName: string; const ANew: boolean = false );
begin
  FDrugList.AddDrug( AATC, ADrugName, ANew );
end;

procedure TDrugChecker.AddResult( const AATC1, AATC2, Name1, Name2: string; const ALevelId: integer; const AText: string);
var
  n: integer;
  objListInteraction: TDrugInteraction;
  objNewInteraction: TDrugInteraction;
begin
  { Look for existing interactions }
  objListInteraction := nil; { Avoid warning during compilation }
  objNewInteraction := TDrugInteraction.Create( FActualInteractions.Count, AATC1, AATC2, ALevelId, AText );
  n := 0;
  while n < FActualInteractions.Count do begin
    objListInteraction := FActualInteractions[n] as TDrugInteraction;
    if objListInteraction.SameAs( objNewInteraction ) then begin
      { Discard this interaction if it was already present }
      FreeAndNil( objNewInteraction );
      break
    end;
    inc( n );
  end;
  { Create an interaction if there were no matches }
  if Assigned( objNewInteraction ) then begin
    FActualInteractions.Add( objNewInteraction );
    objListInteraction := objNewInteraction;
  end;
  Assert( Assigned( objListInteraction ) );
  objListInteraction.Drugs1.AddDrug( AATC1, Name1 );
  objListInteraction.Drugs2.AddDrug( AATC2, Name2 );
end;

procedure TDrugChecker.Clear;
begin
  FActualInteractions.Clear;
  FDrugList.Clear;
end;

function TDrugChecker.HasInteractions(out AOldCount, ANewCount: integer): boolean;
var
  lstOldATC: TStringList;
  lstAllATC: TStringList;
  n: integer;
begin
  Assert( FReady );
  lstAllATC := TStringList.Create;
  lstAllATC.Sorted := true;
  lstAllATC.Duplicates := dupIgnore;
  lstOldATC := TStringList.Create;
  lstOldATC.Sorted := true;
  lstOldATC.Duplicates := dupIgnore;
  try
    with FDrugList do begin
      n := 0;
      while n < Count do begin
         lstAllATC.Add( NormalizedATC( FDrugList.Drug[n].ATC ) );
         if not FDrugList.Drug[n].NewToday then
          lstOldATC.Add( NormalizedATC( FDrugList.Drug[n].ATC ) );
        inc( n );
      end;
    end;
    lstOldATC.Sorted := false;
    lstAllATC.Sorted := false;
    { First add for all old drugs }
    FActualInteractions.Clear;
    FEM.RefreshForATCList( lstOldATC, rtSection );
    AOldCount := FActualInteractions.Count;
    { Tag all interactions as old }
    n := 0;
    while n < FActualInteractions.Count do begin
      with FActualInteractions[n] as TDrugInteraction do
        FOld := true;
      inc( n );
    end;
    { Now refresh for all new drugs }
    FEM.RefreshForATCList( lstAllATC, rtSection );
    ANewCount := FActualInteractions.Count;
    Result := ( ANewCount > 0 );
  finally
    lstOldATC.Free;
    lstAllATC.Free;
  end;
end;

{ TKBInteractions }

procedure TKBInteractionList.AddRow(const AIntId: integer; const AATC1, AATC2: string;
  const ALevelId: integer; const AText: string);
begin
  Self.Add( TKBInteraction.Create( AIntId, AATC1, AATC2, ALevelId, AText ) );
end;

procedure TKBInteractionList.LoadFromFile(const AFileName: string);
var
  lstData: TStringList;
  lstRow: TStringList;
  i: integer;
begin
  lstData := TStringList.Create;
  lstRow := TStringList.Create;
  lstRow.Delimiter := #9;
  lstRow.StrictDelimiter := true;
  try
    lstData.LoadFromFile( AFileName );
    Assert( lstData.Count > 0 );
    for i := 0 to lstData.Count - 1 do begin
      lstRow.DelimitedText := lstData[i];
      AddRow(
        StrToInt( Trim( lstRow[0] ) ),
        Trim( lstRow[1] ), Trim( lstRow[2] ),
        StrToInt( Trim( lstRow[3] ) ),
        Trim( lstRow[4] ) );
    end;
  finally
    lstRow.Free;
    lstData.Free;
  end;
end;

{$IFDEF NewVersion}

procedure TDrugChecker.LoadKnowledgeData( const ALocation: string );
var
  n: integer;
  lstInteractionFile: TStringList;
  lstRow: TStringList;
begin
  lstInteractionFile := TStringList.Create;
  lstRow := TStringList.Create;
  lstRow.Delimiter := #9;
  lstRow.StrictDelimiter := true;
  try
    lstInteractionFile.LoadFromFile( 'interactions.tsv' );
    n := 0;
    while n < lstInteractionFile.Count do begin
      lstRow.DelimitedText := lstInteractionFile[n];
      FKbInteractions.AddRow( n, Trim( lstRow[0] ), Trim( lstRow[1] ), StrToInt( Trim( lstRow[2] ) ), Trim( lstRow[3] ) );
      inc( n );
    end;
  finally
    lstInteractionFile.Free;
    lstRow.Free;
  end;
end;

{$ELSE}

function TDrugChecker.LoadKnowledgeData( const ALocation: string ): boolean;
begin
  FReady := FEM.Load( ALocation );
  Result := FReady;
end;

{$ENDIF}

end.
