unit FLINKlocal;

interface

uses
  { Standard VCL modules }
  Classes, Controls, Dialogs, SysUtils, Math, Forms, Windows,
  { Project related modules }
  FLINKgen, FLINKres,
  { mrlib }
  MrBase, MrStrDb;

type

  TInteractionEvent = procedure( const AATC1, AATC2, AName1, AName2: string;
    const ALevel: integer; const AText: string ) of object;

  TFlinkLocal = class(TObject)
    TabInter: TStrDb;
    TabSubstances: TStrDb;
    TabMetaMembers: TStrDb;
    TabClassification: TStrDb;
  private
    FATC: TStringList;
    FErrors: TStringList;
    FFLINKBase: string;
    FFound: TStringList;
    FKnown: TStringList;
    FOnActivity: TNotifyStringEvent;
    FOnInteraction: TInteractionEvent;
    FOnNoActivity: TNotifyEvent;
    FTimestamp: TDateTime;
    FUnknown: TStringList;
    FValid: boolean;
    function AddLocalInteraction( const AATC1, AATC2: string; const AIsCurrent: boolean): string;
    function FindNextSplit( const ATextToAnalyze: string): integer;
    procedure AddMetagroups( AListATC: TStrings );
    procedure AddParentATCs( AListATC: TStrings);
    procedure RemoveUnderscore( AListATC: TStrings );
    procedure StopActivity;
    procedure WhatsUp(S: string);
  public
    constructor Create;
    destructor Destroy; override;
    function CheckIntegrity: boolean;
    function CreateWordList( AInput: string; const AClear: boolean ): integer;
    function FindOneName( AATC: string): string;
    function Load( const AFLINKbase: string ): boolean;
    function MapCount( const AWord: string ): integer;
    function TransformWordToATCList(var OneWord: string; var AClass: TWordClass; ATCList: TStringList): integer;
    procedure ClearWords;
    procedure FindNames( AATC: string; ANames: TStrings);
    procedure FloppyExport( const ADriveLetter: char = 'A' );
    procedure FloppyImport( const ADriveLetter: char = 'A' );
    procedure RefreshForATCList( AATCList: TStrings; const AResultType: TResultType);
    procedure RefreshForWords( AInputLines: TStringList; ResultType: TResultType);
    property ATCList: TStringList read FATC;
    property Errors: TStringList read FErrors;
    property Known: TStringList read FKnown;
    property OnActivity: TNotifyStringEvent read FOnActivity write FOnActivity;
    property OnInteraction: TInteractionEvent read FOnInteraction write FOnInteraction;
    property OnNoActivity: TNotifyEvent read FOnNoActivity write FOnNoActivity;
    property Timestamp: TDateTime read FTimestamp;
    property Unknown: TStringList read FUnknown;
  end;

const
  TI_ATC1 = 0;
  TI_ATC2 = 1;
  TI_REL = 2;
  TI_DESC = 3;
  TS_SUBST = 1;
  TS_ATC = 0;
  TS_LANG = 2;
  TM_META = 1;
  ATC_UNMAPPED = 'ATC CODE NOT MAPPED';

implementation

resourcestring
  COPY_PREFIX = 'Kopierer ';
  EXPORT_FAILED = 'Det oppstod en feil ved kopiering.\n%d av 3 file(r) ble kopiert.';
  EXPORT_OK  = 'Eksport av databasen gikk fint.';
  IMPORT_FAILED = 'Det oppstod en alvorlig feil under import.\nDatabasen kan v�re ubrukelig.\nPr�v � importere fra en annen diskett.';
  IMPORT_OK = 'Importen gikk fint.\nDatabasen er oppdatert.';
  IMPORT_NOTHING = 'Det oppstod en feil under import.\nDatabasen er uendret og intakt.';
  MSG_IMPORT = 'Importerer filer';
  MSG_EXPORT = 'Eksporterer filer';
  MSG_DISK_PREPARE = 'Sett inn et medium i stasjon ';
  ERR_NAME_MISSING = 'Navn mangler.';

const
  ATC_SPLIT = ' - ';
  CLASS_NAME = 'TEM';
    
function TFlinkLocal.FindNextSplit( const ATextToAnalyze: string): integer;
var
  BreakPoint,
    strlen: integer;
begin
  BreakPoint := 1;
  strlen := Length(ATextToAnalyze);
  while BreakPoint <= strlen do begin
    case ATextToAnalyze[BreakPoint] of
      '('..')',
        '0'..'9',
        '-', '_',
        'a'..'z',
        'A'..'Z',
        '�', '�', '�',
        '�', '�', '�': inc(BreakPoint);
    else
      begin
        Result := BreakPoint;
        exit;
      end;
    end;
  end;
  Result := 0;
end;

procedure TFlinkLocal.RefreshForWords( AInputLines: TStringList; ResultType: TResultType);
begin
  CreateWordList( AInputLines.Text, true );
  FATC.Sorted := false;
  RefreshForATCList( FATC, ResultType);
end;

procedure TFlinkLocal.RemoveUnderscore( AListATC: TStrings );
var
  n: integer;
begin
  n := 0;
  while n < AListATC.Count do begin
    AListATC[n] := StringReplace(AListATC[n], '_', ' ', []);
    inc(n);
  end;
end;

procedure TFlinkLocal.AddParentATCs( AListATC: TStrings );
var
  n: integer;
  SavedCount: integer;
  ATC: string;
begin
  SavedCount := AListATC.Count;
  n := 0;
  while n < SavedCount do begin
    ATC := AListATC[n];
    AListATC.Add(Copy(ATC, 1, 3));
    AListATC.Add(Copy(ATC, 1, 4));
    AListATC.Add(Copy(ATC, 1, 6));
    AListATC.Add(Copy(ATC, 1, 8));
    inc(n);
  end;
end;

procedure TFlinkLocal.AddMetagroups( AListATC: TStrings );
var
  SavedCount: integer;
  n: integer;
begin
 // Add all metagroups the matches are members of
  n := 0;
  SavedCount := AListATC.Count;
  while n < SavedCount do with TabMetamembers do begin
      IndexName := sMemberIdx;
      SetRange([AListATC[n]], [AListATC[n]]);
      First;
      while not EOF do begin
        AListATC.Add(GetString(TM_META));
        Next;
      end;
      inc(n);
    end;
end;

procedure TFlinkLocal.RefreshForATCList( AATCList: TStrings; const AResultType: TResultType);
const
  SplitMark = '-';
  TailMark = '/';
  SecTail = TailMark + 'SEC';
  PriTail = TailMark + 'PRI';
var
  Split1, Split2: integer;
  ATC, ATC1, ATC2: string;
  ListIndex: integer;
  ListPos: integer;
  strLine, strMirror: string;
begin
  FFound.Clear;
  RemoveUnderscore( AATCList );
  AddParentATCs( AATCList );
  AddMetagroups( AATCList );

  with TabInter do begin
    ListIndex := 0;
    while ListIndex < AATCList.Count do begin
      ATC := AATCList[ListIndex];
   // Check first index
      IndexName := sATC1Idx;
      SetRange([ATC], [ATC]);
      First;
      while not EOF do begin
        ATC1 := GetString(TI_ATC1);
        ATC2 := GetString(TI_ATC2);
        strLine := ATC1 + SplitMark + ATC2 + PriTail;
        if FFound.IndexOf( strLine ) = -1 then
          FFound.Add( strLine );
        Next;
      end;
   // Check second index
      IndexName := sATC2Idx;
      SetRange([ATC], [ATC]);
      First;
      while not EOF do begin
        ATC1 := GetString(TI_ATC1);
        ATC2 := GetString(TI_ATC2);
        strLine := ATC1 + SplitMark + ATC2 + SecTail;
        if FFound.IndexOf( strLine ) = -1 then
          FFound.Add( strLine );
        Next;
      end;
      inc(ListIndex);
    end;
  end;
  ListIndex := 0;
  while ListIndex < FFound.Count do begin
    strLine := FFound[ListIndex];
    Split1 := Pos(SplitMark, strLine);
    Split2 := Pos(TailMark, strLine);
    ATC1 := Copy(strLine, 1, Split1 - 1);
    ATC2 := Copy(strLine, Split1 + 1, Split2 - Split1 - 1);
    if AResultType = rtSection then begin
      if Pos(SecTail, strLine) > 0 then begin
        strMirror := StringReplace(strLine, SecTail, PriTail, []);
        if FFound.Find(strMirror, ListPos) then
          AddLocalInteraction(ATC1, ATC2, false);
      end;
    end
    else
      AddLocalInteraction( ATC1, ATC2, false);
    inc(ListIndex);
  end;
end;


function TFlinkLocal.CreateWordList( AInput: string; const AClear: boolean ): integer;
const
  ShortestToInclude = 3;
  LOG_UNKNOWN = CLASS_NAME + '.CreateWordList: Unknown = "%s"';
var
  wc: TWordClass;
  n, m: integer;
  dummy: integer;
  NewWord,
    DoubleName: string;
  WordToLookUp: string;
  spacepos: integer;
  TextToAnalyze: string;
  locInputLines: TStringList;
  smallATCList: TStringList;
  DoubleWords: TStringList;
begin
  if AClear then ClearWords;
  locInputLines := TStringList.Create;
  locInputLines.Text := AInput;

  locInputLines.Text := AnsiUppercase(locInputLines.Text);

 // Make list of double words                                               .

  DoubleWords := TStringList.Create;
  with TabSubstances do begin
    First;
    while not EOF do begin
      DoubleName := GetString(TS_SUBST);
      if Pos(' ', Trim(DoubleName)) <> 0 then
        DoubleWords.Add(AnsiUppercase(DoubleName));
      Next;
    end;
  end;

 // Replace spaces in double words with underscore                          .

  m := 0;
  while m < DoubleWords.Count do begin
    DoubleName := DoubleWords[m];
    NewWord := StringReplace(DoubleName, ' ', '_', [rfReplaceAll]);
    locInputLines.Text := StringReplace(locInputLines.Text, DoubleName, NewWord, [rfReplaceAll]);
    inc(m);
  end;

 // Set up lists                                                            .
  FATC.Clear;
  FATC.Sorted := true;
  FATC.Duplicates := dupIgnore;

  smallATClist := TStringList.Create;
  smallATClist.Sorted := true;
  smallATClist.Duplicates := dupIgnore;
  n := 0;

  { Look at input                                                           .
    Use Split function? }

  while n < locInputLines.Count do begin
    TextToAnalyze := locInputLines[n] + '.';
    spacepos := FindNextSplit(TextToAnalyze);
    while spacepos <> 0 do begin
      if spacepos > ShortestToInclude then begin
        WordToLookUp := Copy(TextToAnalyze, 1, spacepos - 1);
        if not FUnknown.Find(WordToLookUp, dummy) then begin
          if TransformWordToATCList(WordToLookUp, wc, SmallATClist) = 0 then
            FUnknown.Add(WordToLookUp)
          else begin
            FKnown.Add(WordToLookUp);
            if wc=wcATC then
              FATC.Add( NormalizedATC( WordToLookUp ) + ATC_SPLIT + FindOneName( WordToLookUp ) + '*' )
            else begin
              m := 0;
              while (m < SmallATCList.Count) do begin
                FATC.Add( SmallATCList[m] + ATC_SPLIT + WordToLookUp);
                inc(m);
              end;
            end;
          end;
        end;
      end;
      Delete(TextToAnalyze, 1, spacepos);
      spacepos := FindNextSplit(TextToAnalyze);
    end;
    inc(n);
  end;

 // Free lists                                                              .
  locInputLines.Free;
  smallATClist.Free;
  DoubleWords.Free;
  Result := FKnown.Count;
  if FUnknown.Count > 0 then
    LOG.Event( LOG_UNKNOWN, [FUnknown.CommaText] );
end;


procedure TFlinkLocal.FindNames( AATC: string; ANames: TStrings );
const
  LOG_NO_TABLE = '%s.FindNames: No table found';
var
  thisDB: TStrDb;
begin
  NormalizeATC( AATC );
  if Assigned( TabSubstances ) then
    thisDB := TabSubstances
  else
    thisDB := TabClassification;
  if not Assigned( thisDB ) then
    raise Exception.CreateFmt( LOG_NO_TABLE, [ClassName] );
  ANames.Clear;
  thisDB.IndexName := sATCIdx;
  thisDB.SetRange( [ AATC ], [ AATC ] );
  thisDB.First;
  while not thisDB.EOF do
  begin
    ANames.Add( thisDB.GetString( TS_SUBST ) );
    thisDB.Next;
  end;
end;


function TFlinkLocal.FindOneName( AATC: string ): string;
begin
  NormalizeATC( AATC );
  with TabSubstances do begin
    IndexName := sATCIdx;
    if FindKey( [ AATC ] ) then
      Result := GetString(TS_SUBST)
    else
      Result := ATC_UNMAPPED;
  end;
end;


function TFlinkLocal.AddLocalInteraction( const AATC1, AATC2: string; const AIsCurrent: boolean): string;
var
  Name1, Name2, strDesc, strLine: string;
  iRel: integer;
begin
  with TabSubstances do begin
    Result := '';
    if (Length(AATC1) = 0)
      or (Length(AATC2) = 0) then
      raise Exception.Create('AddLocalInteraction: ATC.Length=0');

    IndexName := sATCIdx;
    if FindKey([AATC1]) then
      Name1 := GetString(TS_SUBST)
    else
      Name1 := '*';
    if FindKey([AATC2]) then
      Name2 := GetString(TS_SUBST)
    else
      Name2 := '*';
  end;
  with TabInter do begin
    if not AIsCurrent then begin
      IndexName := sATC1Idx;
      if FindKey([AATC1, AATC2]) then begin
        iRel := StrToInt( GetString( TI_REL ) );
        strDesc := GetString( TI_DESC );
      end
      else begin
        iRel := 4;
        strDesc := 'Interaction text and level not found';
      end;
    end
    else begin
      iRel := StrToInt( GetString( TI_REL ) );
      strDesc := GetString( TI_DESC );
    end;
    strLine := CreateFLINKresultLine(AATC1, AATC2, Name1, Name2, IntToStr( iRel ), strDesc );
    if Assigned( FOnInteraction ) then
      FOnInteraction( AATC1, AATC2, Name1, Name2, iRel, strDesc )
    {$IFDEF XmlParser}
    else
      FLINKresult.Add( strLine );
    {$ENDIF}
  end;
end;


function TFlinkLocal.TransformWordToATCList(var OneWord: string; var AClass: TWordClass; ATCList: TStringList): integer;
var
  ATC: string;
  SavedWord: string;
begin
  SavedWord := OneWord;
  with TabSubstances do begin
  // Suspend update of visual controls
  // Action( tSubst, dbDisableControls );
  // Clear list, and ignore duplicates
    ATCList.Clear;
    ATCList.Sorted := true;
    ATCList.Duplicates := dupIgnore;
    ATC := ATCFromNode(OneWord);
    AClass := GetWordClass( ATC );
    case AClass of
      wcATC, wcMeta:
        begin
     // The word is regarded as an ATC code
          OneWord := NormalizedATC(ATC);
          SavedWord := OneWord;
          IndexName := sATCIdx;
          if FindKey([OneWord]) then
            ATCList.Add(OneWord);
        end;
      wcNone, wcWord:
        begin
          IndexName := sSubstIdx;
          SetRange([OneWord], [OneWord]);
          First;
          while not EOF do begin
      // find all ATCs where this substance appears
            ATCList.Add(GetString(TS_ATC));
            Next;
          end;
          CancelRange;
        end;
    end;
  // ATC codes is now in ATCList
  // Add ATC groups for possible inherited interactions
  end;
  Result := ATCList.Count;
  OneWord := StringReplace(SavedWord, ' ', '_', [rfReplaceAll]);
end;


destructor TFlinkLocal.Destroy;
begin
  FErrors.Free;
  FKnown.Free;
  FUnknown.Free;
  inherited;
end;


procedure TFlinkLocal.WhatsUp( S: string );
begin
  if Assigned( FOnActivity ) then FOnActivity( s );
end;


procedure TFlinkLocal.StopActivity;
begin
  if Assigned( FOnNoActivity ) then FOnNoActivity( Self );
end;


constructor TFlinkLocal.Create;
begin
  FTimestamp := 0;
  FOnActivity := nil;
  FErrors := TStringList.Create;
  FKnown := TStringList.Create;
  FUnknown := TStringList.Create;
  FUnknown.Sorted := true;
  FUnknown.Duplicates := dupIgnore;
  FKnown.Sorted := true;
  FKnown.Duplicates := dupIgnore;
  FFound := TStringList.Create;
  FFound.Sorted := true;
  FFound.Duplicates := dupError;
  FATC := TStringList.Create;
end;

function TFlinkLocal.Load( const AFLINKbase: string): boolean;
const
  PROC_NAME = CLASS_NAME + '.Load: ';
  LOG_BASE = PROC_NAME + 'FLINKBase = %s' + TAG_BLUE;
begin
  FErrors.Clear;
  WhatsUp( 'Creating tables' );
  if TabSubstances <> nil then FreeAndNil( TabSubstances );
  TabSubstances := TStrDb.Create( 'Substances', [fnATC, fnSubst, fnLang]);
  if TabInter <> nil then FreeAndNil( TabInter );
  TabInter := TStrDb.Create( 'Interactions', [fnATC1, fnATC2, fnRel, fnDesc]);
  if TabMetaMembers <> nil then FreeAndNil( TabMetaMembers );
  TabMetaMembers := TStrDb.Create( 'MetaMembers', [fnMemberATC, fnMetaATC]);
  if TabClassification <> nil then FreeAndNil( TabClassification );
  TabClassification := TStrDb.Create( 'Classification', [fnATC, fnSubst]);

  WhatsUp( 'Resolving path' );
  if AFLINKbase = EmptyStr then
    FFLINKbase := ExtractFilePath( Application.ExeName )
  else
    FFLINKbase := AFLINKbase;
  FFLINKBase := IncludeTrailingPathDelimiter( FFLINKBase );
  FileAge( FFLINKbase + 'interactions.tsv', FTimestamp );

  if Assigned( LOG ) then
    LOG.Event( LOG_BASE, [FFLINKBase] );

  WhatsUp( 'Loading substances' );
  if not TabSubstances.Load( FFLINKbase) then FErrors.Add('Substances not found');
  WhatsUp( 'Loading interactions' );
  if not TabInter.Load( FFLINKbase) then FErrors.Add('Interactions not found');
  WhatsUp( 'Loading metamembers' );
  if not TabMetaMembers.Load( FFLINKbase) then FErrors.Add('MetaMembers not found');
  WhatsUp( 'Loading classification' );
  TabClassification.Load( FFLINKbase);

  //----------------------------------------------------------------------
  // Genererer indekser

  WhatsUp('Creating substance index');
  with TabSubstances do begin
    SetFieldNames([fnATC, fnSubst, fnLang]);
    CreateIndex([TS_ATC, TS_LANG], sATCIdx);
    CreateIndex([TS_SUBST, TS_LANG], sSubstIdx);
  end;

  WhatsUp('Creating interaction index');
  with TabInter do begin
    SetFieldNames([fnATC1, fnATC2, fnRel, fnDesc]);
    CreateIndex([GetFieldNo(fnATC1), GetFieldNo(fnATC2)], sATC1Idx);
    CreateIndex([TI_ATC2, TI_ATC1], sATC2Idx);
  end;

  WhatsUp('Creating metamember index');
  with TabMetaMembers do begin
    SetFieldNames([fnMemberATC, fnMetaATC]);
    CreateIndex([GetFieldNo(fnMemberATC), GetFieldNo(fnMetaATC)], sMemberIdx);
  end;

  WhatsUp('Creating classification index');
  with TabClassification do begin
    SetFieldNames([fnATC, fnSubst, fnLang]);
    CreateIndex([TS_ATC, TS_LANG], sATCIdx);
    CreateIndex([TS_SUBST, TS_LANG], sSubstIdx);
  end;

  TabSubstances.IndexName := sATCIdx;
  TabClassification.IndexName := sATCIdx;
  TabMetaMembers.IndexName := '';
  TabInter.IndexName := sATC1Idx;
  Result := ( TabInter.Count > 50 ) and ( TabSubstances.Count > 500 ) and (FErrors.Text = '')
  and CheckIntegrity;
  WhatsUp('Loading finished');
  StopActivity;
end;


function TFlinkLocal.MapCount(const AWord: string): integer;
begin
  Result := 0;
  with TabSubstances do begin
    IndexName := sSubstIdx;
    SetRange([AWord], [AWord]);
    First;
    while not EOF do begin
      inc( Result );
      Next;
    end;
    CancelRange;
  end;
end;

const
  DRIVE_POSTFIX = ':\';

procedure TFlinkLocal.FloppyExport( const ADriveLetter: char );
var
  iFilesCopied: integer;
  strDest: string;
  strLetter: string;
begin
  iFilesCopied := 0;
  strLetter := ADriveLetter + DRIVE_POSTFIX;
  if MessageDlg( MSG_DISK_PREPARE + strLetter, mtConfirmation, [mbOk,mbCancel], 0 ) = mrOK then
    try
      LOG.Event( MSG_EXPORT );
      WhatsUp( MSG_EXPORT );
      strDest := strLETTER + ExtractFileName( TabSubstances.Filename );
      WhatsUp( COPY_PREFIX  + strDest );
      if CopyFile( pChar( TabSubstances.Filename ), pChar( strDest ), false ) then
        inc( iFilesCopied );
      strDest := strLETTER + ExtractFileName( TabMetaMembers.Filename );
      WhatsUp( COPY_PREFIX  + strDest );
      if CopyFile( pChar( TabMetaMembers.Filename ), pChar( strDest ), false ) then
        inc( iFilesCopied );
      strDest := strLETTER + ExtractFileName( TabInter.Filename );
      WhatsUp( COPY_PREFIX  + strDest );
      if CopyFile( pChar( TabInter.Filename ), pChar( strDest ), false ) then
        inc( iFilesCopied );
      StopActivity;
      if iFilesCopied <> 3 then
        LOG.Event( EXPORT_FAILED, [iFilesCopied], ltError )
      else
        LOG.Event( EXPORT_OK, ltMessage );
    except on E:Exception do
      LOG.Event( E.Message, ltException );
    end;
  StopActivity;
end;


procedure TFlinkLocal.FloppyImport( const ADriveLetter: char );
var
  iFilesCopied: integer;
  strSrc: string;
  strLetter: string;
begin
  strLetter := ADriveLetter + DRIVE_POSTFIX;
  iFilesCopied := 0;
  try
    LOG.Event( MSG_IMPORT, ltInfo );
    WhatsUp( MSG_IMPORT );
    strSrc := strLETTER + ExtractFileName( TabSubstances.Filename );
    WhatsUp( COPY_PREFIX  + strSrc );
    if CopyFile( pChar( strSrc ), pChar( TabSubstances.Filename ), false ) then
      inc( iFilesCopied );
    strSrc := strLETTER + ExtractFileName( TabMetaMembers.Filename );
    WhatsUp( COPY_PREFIX  + strSrc );
    if CopyFile( pChar( strSrc ), pChar( TabMetaMembers.Filename ), false ) then
      inc( iFilesCopied );
    strSrc := strLETTER + ExtractFileName( TabInter.Filename );
    WhatsUp( COPY_PREFIX  + strSrc );
    if CopyFile( pChar( strSrc ), pChar( TabInter.Filename ), false ) then
      inc( iFilesCopied );
  except on E:Exception do
    LOG.Event( IMPORT_FAILED + '\nMelding: ' + E.Message, ltException );
  end;

  StopActivity;
  case iFilesCopied of
    3:
      begin
        Load( ExtractFilePath( TabSubstances.Filename ) );
        LOG.Event( IMPORT_OK, ltMessage );
      end;
    2,1: LOG.Event( IMPORT_FAILED, ltError );
    0: LOG.Event( IMPORT_NOTHING, ltError );
  end;
end;

function TFlinkLocal.CheckIntegrity: boolean;
const
  PROC_NAME = CLASS_NAME + '.CheckIntegrity: ';
  LOG_OK = PROC_NAME + 'Ok';
  LOG_MISSING = PROC_NAME + 'Missing "%s"' + TAG_RED;
var
  i: integer;
  strErrMsg: string;
  lstNames: TSTringList;
begin
  FValid := false;
  lstNames := TStringList.Create;
  lstNames.Sorted := true;
  lstNames.Duplicates := dupIgnore;
  with TabInter do try
    First;
    while not EOF do
    begin
      for i := 0 to 1 do
        FindNames( GetString(i), lstNames );
      if lstNames.Count = 0 then
      begin
        LOG.Event( LOG_MISSING, [ TabInter[ItemIndex] ] );
        strErrMsg := TabInter[ItemIndex] + ' ' + ERR_NAME_MISSING;
        if FErrors.IndexOf( strErrMsg )=-1 then
          FErrors.Add( strErrMsg );
      end;
      Next;
    end;
  except on E:Exception do
    FErrors.Add( E.Message );
  end;
  lstNames.Free;
  FValid := ( FErrors.Count = 0 ) and ( TabInter.Count > 1000 );
  if FValid and Assigned( LOG ) then
    LOG.Event( LOG_OK );
  Result := FValid;
end;

procedure TFlinkLocal.ClearWords;
begin
  FKnown.Clear;
  FUnknown.Clear;
  FATC.Clear;
end;

end.
