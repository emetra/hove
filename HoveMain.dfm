object frmHoveMain: TfrmHoveMain
  Left = 0
  Top = 0
  BorderWidth = 8
  Caption = 'HoveMain'
  ClientHeight = 358
  ClientWidth = 445
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  PixelsPerInch = 96
  TextHeight = 13
  object lblIntHeader: TLabel
    AlignWithMargins = True
    Left = 3
    Top = 84
    Width = 439
    Height = 13
    Align = alTop
    Caption = 'Interaksjoner'
    ExplicitLeft = 0
    ExplicitTop = 81
    ExplicitWidth = 65
  end
  object lbInteractions: TListBox
    AlignWithMargins = True
    Left = 3
    Top = 103
    Width = 439
    Height = 72
    Align = alTop
    ItemHeight = 13
    TabOrder = 0
    ExplicitTop = 97
    ExplicitWidth = 455
  end
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 445
    Height = 81
    Align = alTop
    BevelOuter = bvNone
    Caption = 'Panel1'
    TabOrder = 1
    ExplicitWidth = 461
    object Panel2: TPanel
      Left = 185
      Top = 0
      Width = 260
      Height = 81
      Align = alClient
      BevelOuter = bvNone
      Caption = 'Panel2'
      TabOrder = 0
      ExplicitWidth = 276
      object lblTodayHeader: TLabel
        AlignWithMargins = True
        Left = 3
        Top = 3
        Width = 254
        Height = 13
        Align = alTop
        Caption = 'Dagens medisiner'
        ExplicitLeft = 0
        ExplicitTop = 0
        ExplicitWidth = 84
      end
      object lbToday: TListBox
        AlignWithMargins = True
        Left = 3
        Top = 22
        Width = 254
        Height = 56
        Align = alClient
        ItemHeight = 13
        Items.Strings = (
          'B01AC06 Albyl-E')
        TabOrder = 0
        ExplicitTop = 16
        ExplicitWidth = 270
        ExplicitHeight = 62
      end
    end
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 185
      Height = 81
      Align = alLeft
      BevelOuter = bvNone
      Caption = 'Panel3'
      TabOrder = 1
      object lblOldHeader: TLabel
        AlignWithMargins = True
        Left = 3
        Top = 3
        Width = 179
        Height = 13
        Align = alTop
        Caption = 'Faste medisiner'
        ExplicitLeft = 0
        ExplicitTop = 0
        ExplicitWidth = 75
      end
      object lbOld: TListBox
        AlignWithMargins = True
        Left = 3
        Top = 22
        Width = 179
        Height = 56
        Align = alClient
        ItemHeight = 13
        Items.Strings = (
          'B01AA03 Marevan'
          'M01AC01 Brexidol')
        TabOrder = 0
        ExplicitTop = 16
        ExplicitHeight = 62
      end
    end
  end
  object panBottom: TPanel
    Left = 0
    Top = 325
    Width = 445
    Height = 33
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 2
    ExplicitTop = 341
    ExplicitWidth = 461
    object lblInfo: TLabel
      AlignWithMargins = True
      Left = 84
      Top = 3
      Width = 358
      Height = 27
      Align = alClient
      AutoSize = False
      Caption = 'Informasjon kommer her'
      EllipsisPosition = epEndEllipsis
      Layout = tlCenter
      ExplicitLeft = 86
      ExplicitTop = 4
      ExplicitWidth = 290
      ExplicitHeight = 33
    end
    object btnCheck: TButton
      AlignWithMargins = True
      Left = 3
      Top = 3
      Width = 75
      Height = 27
      Align = alLeft
      Caption = 'Sjekk n'#229
      TabOrder = 0
      OnClick = btnCheckClick
    end
  end
  object Panel5: TPanel
    Left = 0
    Top = 178
    Width = 445
    Height = 147
    Align = alClient
    BevelOuter = bvNone
    Caption = 'Panel5'
    TabOrder = 3
    ExplicitTop = 172
    ExplicitWidth = 461
    ExplicitHeight = 169
    object lbLog: TListBox
      AlignWithMargins = True
      Left = 3
      Top = 3
      Width = 439
      Height = 141
      Align = alClient
      ItemHeight = 13
      TabOrder = 0
      ExplicitWidth = 455
      ExplicitHeight = 163
    end
  end
end
