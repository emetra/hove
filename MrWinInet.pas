unit MrWinInet;

interface

uses
  Emetra.Interfaces.Logging,
  Emetra.Interfaces.Progress,
  Classes, Forms, Graphics, Registry, StdCtrls, SysUtils, Windows, WinInet;

resourcestring
  EXC_URL_FAILED = 'Kunne ikke koble til URL: %s';
  EXC_NO_INET =
    'Den �nskede TCP/IP tilkobling via WININET.DLL er ikke tilgjengelig.\n ' +
    'Du b�r kanskje f� en tekniker til � se p� internett-innstillingene.\n ' +
    'Merk: Hvis du avbr�t en oppringt linje er denne feilen som ventet!';
  USR_AGENT =
    'Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; SV1; .NET CLR 1.1.4322; '+
    'NHN-IkkeSensitiv 1.IE.09; .NET CLR 2.0.50727)';
  HTTP_10 = 'HTTP/1.0';
  APP_FORM_URL = 'application/x-www-form-urlencoded';
  MULTI_FORM_DATA ='multipart/form-data';

type

  TInfoEvent = procedure( Sender: TObject; s: string ) of object;

  TMrInet = class( TObject)
    private
      FStatus: IStatus;
      FAccessType: DWORD;
      FInternetOpenHandle: HINTERNET;
      FUrlHandle: HINTERNET;
      FFlags: dword;
      FBytesRead: dword;
      FResponseSize: dword;
      FOnProgress: TNotifyEvent;
      FBuffer: array[0..8191] of AnsiChar;
      FServerResponse: string;
      procedure AddBytes;
      procedure ClearBuffer;
      procedure Connect;
      procedure ResetCounters;
      procedure CloseHandles;
      procedure Set_StatusText( const s: string );
    public
      constructor Create( AStatus: IStatus = nil );
      destructor Destroy; override;
      function GetString( const AURL: string ): string; overload;
      function GetString( const AHost, APath: string; const APort: integer; lpszPostData, lpszHeaders: PChar ): string; overload;
      function GetProxy( var ProxyEnabled: boolean ): string;
      function Download( const AURL: string; const ADestFile: string ): boolean;
      property AccessType: DWORD read FAccessType write FAccessType;
      property ResponseSize: DWORD read FResponseSize;
      property StatusText: string write Set_StatusText;
      property OnProgress: TNotifyEvent read FOnProgress write FOnProgress;
    end;

implementation

resourcestring
  TXT_CONNECT_INET       = 'Oppretter internettforbindelse ...';
  TXT_CHECK_PROXY        = 'Sjekker ProxyServer innstillinger';
  TXT_BYTE_COUNT         = 'Mottatt %d bytes s� langt ...';
  TXT_RESPONSE_SIZE      = 'Responsen inneholdt totalt %d bytes.';
  TXT_CONNECT_HOST_PORT  = 'Kobler til vert "%s" via port %d ...';
  TXT_POST_REQUEST       = '�pner for POST til "%s" ...';
  TXT_POST_DATA          = 'Sender %d bytes til "%s" ...';
  TXT_HANDLES_CLOSED     = 'Har koblet ned internettforbindelsen.';
  TXT_WAITING            = 'Venter p� svar...';

constructor TMrInet.Create( AStatus: IStatus );
begin
  Assert( Assigned( AStatus ) );
  FStatus := AStatus;
  FAccessType := INTERNET_OPEN_TYPE_PRECONFIG;
  FFlags :=  INTERNET_FLAG_RELOAD + INTERNET_FLAG_PRAGMA_NOCACHE + INTERNET_FLAG_NO_CACHE_WRITE + INTERNET_FLAG_NO_UI;
end;

destructor TMrInet.Destroy;
begin
  FStatus := nil;
  if FInternetOpenHandle <> nil then
    InternetCloseHandle( FInternetOpenHandle );
end;

procedure TMrInet.AddBytes;
begin
  inc( FResponseSize, FBytesRead );
  StatusText := Format( TXT_BYTE_COUNT, [FResponseSize] );
  if Assigned( FOnProgress ) then
    FOnProgress( Self );
end;

procedure TMrInet.ClearBuffer;
begin
  FillChar(FBuffer, SizeOf(FBuffer), 0);
end;

procedure TMrInet.CloseHandles;
begin
  StatusText := Format( TXT_RESPONSE_SIZE,[FResponseSize] );
  if FUrlHandle <> nil then
    InternetCloseHandle(FUrlHandle);
  if FInternetOpenHandle <> nil then
    InternetCloseHandle(FInternetOpenHandle);
  StatusText := TXT_HANDLES_CLOSED;
end;

procedure TMrInet.Connect;
begin
  ResetCounters;
  FInternetOpenHandle := nil;
  if Assigned( FStatus ) then
    FStatus.Info  := TXT_CONNECT_INET;
  if InternetAttemptConnect( 0 ) = ERROR_SUCCESS then
    FInternetOpenHandle := InternetOpen( pChar( USR_AGENT ), FAccessType, nil, nil, 0 );
end;


function TMrInet.GetProxy( var ProxyEnabled: boolean ): string;
var
	Reg: TRegistry;
begin
  if Assigned( FStatus ) then
    FStatus.Info := TXT_CHECK_PROXY;
	Reg := TRegistry.Create;
  Result := '';
	try
		Reg.RootKey := HKEY_CURRENT_USER;
		if Reg.OpenKey('Software\Microsoft\Windows\CurrentVersion\Internet Settings', True )
		then Result := Lowercase( Reg.ReadString('ProxyServer') );
		ProxyEnabled := Reg.ReadBool( 'ProxyEnable' );
	finally
		Reg.CloseKey;
		Reg.Free;
	end;
end;


function TMrInet.GetString( const AURL: string ): string;
const
  LOG_ERROR = '%s.GetString(): Failed with Code=%d %s, URL=%s';
begin
  ResetCounters;
  Connect;
  if Assigned( FStatus ) then
    FStatus.Info := AURL;
  try
    FUrlHandle :=  InternetOpenUrl( FInternetOpenHandle, pChar( AURL ), '', 0, FFlags, 0 );
    if FUrlHandle=nil then
      raise EInOutError.CreateFmt( LOG_ERROR, [ClassName,GetLastError,SysErrorMessage(GetLastError),AURL] );
    while true do
    begin
      ClearBuffer;
      if not InternetReadFile( FUrlHandle, @FBuffer, SizeOf( FBuffer ), FBytesRead ) or ( FBytesRead = 0 ) then
        break;
      AddBytes;
      FServerResponse := FServerResponse + string( Copy( FBuffer, 0, FBytesRead ) );
    end;
  finally
    CloseHandles;
  end;
  Result := FServerResponse;
end;


function TMrInet.Download( const AURL: string; const ADestFile: string ): boolean;
var
  F: TFileStream;
begin
  ResetCounters;
  Result := false;
  F := nil;
  try
    Connect;
    if Assigned(FInternetOpenHandle) then
    begin
      F := TFileStream.Create( ADestFile, fmCreate );
      if Assigned( FStatus ) then
        FStatus.Info := AURL;
      FUrlHandle := InternetOpenUrl( FInternetOpenHandle, PChar(AURL), nil, 0, INTERNET_FLAG_DONT_CACHE, 0);
      if Assigned(FUrlHandle) then
      begin
        while true do
        begin
          ClearBuffer;
          if not InternetReadFile(FUrlHandle, @FBuffer, SizeOf(FBuffer), FBytesRead)
          or ( FBytesRead = 0 ) then
            break;
          F.WriteBuffer( FBuffer, FBytesRead );
          AddBytes;
        end;
      end
      else
        raise Exception.CreateFmt(EXC_URL_FAILED, [AURL]);
    end
    else
      { NetHandle is not valid. Raise an exception }
      raise Exception.Create(EXC_NO_INET);
    Result := true;
  finally
    CloseHandles;
    F.Free;
  end;
end;

function TMrInet.GetString(const AHost, APath: string; const APort: integer; lpszPostData, lpszHeaders: PChar ): string;
var
  PostHandle: HINTERNET;
  lpszHost: array[0..255] of char;
  lpszPath: array[0..255] of char;
begin
  ResetCounters;
  PostHandle := nil;
  StrPCopy( lpszHost, AHost );
  StrPCopy( lpszPath, APath );
  try
    Connect;
    StatusText := Format( TXT_CONNECT_HOST_PORT, [AHost,APort] );
    PostHandle := InternetConnect(FInternetOpenHandle, lpszHost, APort, '', 'HTTP/1.1',INTERNET_SERVICE_HTTP, 0, 0 );
    If PostHandle = nil then
      raise Exception.CreateFmt( 'InternetConnect to %s returned nil', [AHost] );
    StatusText := Format( TXT_POST_REQUEST, [APath] );
    FUrlHandle := HttpOpenRequest(PostHandle, 'POST', lpszPath, 'HTTP/1.1', '', nil, INTERNET_FLAG_DONT_CACHE, 0);
    If FUrlHandle = nil then
      raise Exception.CreateFmt( 'HttpOpenRequest to %s returned nil', [APath] );
    HttpAddRequestHeaders( FUrlHandle, lpszHeaders, StrLen( lpszHeaders ),
      HTTP_ADDREQ_FLAG_REPLACE OR HTTP_ADDREQ_FLAG_ADD );
    StatusText := Format( TXT_POST_DATA, [StrLen(lpszPostData),APath] );
    HttpSendRequest( FUrlHandle, '', 0, lpszPostData,StrLen( lpszPostdata) );
    StatusText := TXT_WAITING;
    while true do
    begin
      ClearBuffer;
      if ( not InternetReadFile(FUrlHandle, @FBuffer, SizeOf(FBuffer), FBytesRead) )
      or ( FBytesRead = 0 ) then
        break;
      FServerResponse := FServerResponse + string( Copy( StrPas( FBuffer ), 1, FBytesRead ) );
      AddBytes;
    end;
  finally
    if Assigned( PostHandle ) then
      InternetCloseHandle( PostHandle );
    CloseHandles;
  end;
  Result := FServerResponse;
end;

procedure TMrInet.ResetCounters;
begin
  FResponseSize := 0;
  FBytesRead := 0;
  FServerResponse := EmptyStr;
  if Assigned( FOnProgress ) then
    FOnProgress( Self );
end;

procedure TMrInet.Set_StatusText(const s: string);
begin
  if Assigned(FStatus) then
    FStatus.Info := s;
end;

end.
