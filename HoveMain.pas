unit HoveMain;

interface

uses
  DrugChecker,
  MrBase, MrTimer,
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ExtCtrls;

type
  TfrmHoveMain = class(TForm)
    lbLog: TListBox;
    lbInteractions: TListBox;
    lbOld: TListBox;
    lbToday: TListBox;
    lblIntHeader: TLabel;
    lblOldHeader: TLabel;
    lblTodayHeader: TLabel;
    lblInfo: TLabel;
    panBottom: TPanel;
    btnCheck: TButton;
    Panel1: TPanel;
    Panel2: TPanel;
    Panel3: TPanel;
    Panel5: TPanel;
    procedure btnCheckClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
  private
    { Private declarations }
    FDrugChecker: TDrugChecker;
  public
    { Public declarations }
  end;

var
  frmHoveMain: TfrmHoveMain;

implementation

{$R *.dfm}

procedure TfrmHoveMain.FormCreate(Sender: TObject);
begin
  LOG.Lines := lbLog.Items;
  FDrugChecker := TDrugChecker.Create;
  FDrugChecker.LoadKnowledgeData( ExtractFilePath( Application.ExeName ) );
end;

procedure TfrmHoveMain.FormDestroy(Sender: TObject);
begin
  LOG.Lines := nil;
  FDrugChecker.Free;
end;

procedure TfrmHoveMain.btnCheckClick(Sender: TObject);
const
  LOG_CLICK = '%s.btnCheckClick: Adding ATC="%s" Name="%s" New="%s"';
var
  n: Integer;
  lb: TListBox;
  bNewDrug: boolean;
  iOld,iNew: integer;
  lstRow: TStringList;
  strLine: string;
  dStartAt: double;
begin
  lbInteractions.Clear;
  lstRow := TStringList.Create;
  try
    dStartAt := GetHiTickCount;
    for bNewDrug := false to true do
    begin
      n := 0;
      if bNewDrug then
        lb := lbToday
      else
        lb := lbOld;
      while n < lb.Count do
      begin
        lstRow.DelimitedText := lb.Items[n];
        LOG.Event( LOG_CLICK,
          [ClassName,lstRow[0],lstRow[1],BoolToStr(bNewDrug,true)] );
        FDrugChecker.AddDrug( lstRow[0], lstRow[1], bNewDrug );
        inc( n );
      end;
    end;
    if FDrugChecker.HasInteractions( iOld, iNew ) then
    begin
      lblInfo.Caption := Format( 'Har %d interaksjoner fra f�r, og %d med dagens preparater', [iOld,iNew] );
      n := 0;
      while n < FDrugChecker.ActualInteractions.Count do begin
        with FDrugChecker.ActualInteractions[n] as TDrugInteraction do
        begin
          case Old of
            true: strLine := 'GML: ';
            false: strLine := 'NYE: ';
          end;
          lbInteractions.Items.Add( strLine + ATC1 + ' (' + Drugs1.Names + ') + ' + ATC2 + ' (' + Drugs2.Names + ') => ' + Text );
        end;
        inc( n );
      end;
    end
    else
      lblInfo.Caption := 'Ingen interaksjoner';
    LOG.Event( 'CheckComplete: %s',  [GetTimeStrSince( dStartAt)] );
  finally
    lstRow.Free;
  end;
end;

end.
