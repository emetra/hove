unit TestFireMain;

interface

uses
  Emetra.Druid.Downloader,
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants,
  Xml.XmlDom,
  FMX.Types, FMX.Controls, FMX.Forms, FMX.Dialogs;

type
  TDownloadState = (sNone, sDownloading, sError, sComplete);

  TfrmFireMonkeyDownload = class(TForm, IStatusInfo)
    lblInfo: TLabel;
    Timer1: TTimer;
    ProgressBar1: TProgressBar;
    StyleBook1: TStyleBook;
    procedure DownloadData(Sender: TObject);
    procedure ExitProgram(Sender: TObject);
    procedure FormActivate(Sender: TObject);
  private
    { Private declarations }
    FState: TDownloadState;
    procedure SetInfo(const s: string);
    procedure SetProgress(const APercent: integer);
  public
    { Public declarations }
  end;

var
  frmFireMonkeyDownload: TfrmFireMonkeyDownload;

implementation

{$R *.fmx}

procedure TfrmFireMonkeyDownload.DownloadData(Sender: TObject);
var
  download: TDruidDownloader;
begin
  Timer1.OnTimer := nil;
  Timer1.Enabled := false;
  FState := sDownloading;
  download := TDruidDownloader.Create;
  try
    download.Execute(Self);
    Timer1.OnTimer := ExitProgram;
    Timer1.Enabled := true;
    FState := sComplete;
  except
      on E: EDOMParseError do
      begin
      FState := sError;
      SetInfo(Format('Ingen forbindelse: %s', [E.Message]));
      end;
    on E: Exception do
    begin
      FState := sError;
      SetInfo(Format('Nedlasting feilet(%s): ', [E.ClassName, E.Message]));
    end;
  end;
  download.Free;
end;

procedure TfrmFireMonkeyDownload.ExitProgram(Sender: TObject);
begin
  Timer1.OnTimer := nil;
  Timer1.Enabled := false;
  Close;
end;

procedure TfrmFireMonkeyDownload.FormActivate(Sender: TObject);
begin
  if FState = sNone then
  begin
    Timer1.OnTimer := DownloadData;
    Timer1.Enabled := true;
  end;
end;

procedure TfrmFireMonkeyDownload.SetInfo(const s: string);
begin
  lblInfo.Text := s;
  Application.ProcessMessages;
end;

procedure TfrmFireMonkeyDownload.SetProgress(const APercent: integer);
begin
  ProgressBar1.Value := APercent;
end;

end.
