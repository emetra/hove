unit Main.Druid.Download;

interface

uses
  Emetra.Interfaces.Progress,
  Emetra.Druid.Downloader,
  MrWinInet,
  XmlIntf, Registry,
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ComCtrls, StdCtrls, ExtCtrls;

type
  TfrmDruidDownload = class(TForm, IStatusInfo, IStatus )
    ProgressBar1: TProgressBar;
    lblInfo: TLabel;
    Timer1: TTimer;
    procedure DownloadNow(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
    FDownloader: TDruidDownloader;
    FHttpConnection: TMrInet;
    FLocalPath: string;
    procedure SetInfo( const s: string );
    function GetInfo: string;
    procedure SetProgress( const APercent: integer );
    procedure OnLoad( ADocument: IXmlDocument; const AUrl: string );
    procedure Done;
    procedure FindKey;
    procedure CloseApp( Sender: TObject );
  public
    { Public declarations }
  end;

var
  frmDruidDownload: TfrmDruidDownload;

implementation

{$R *.dfm}

procedure TfrmDruidDownload.DownloadNow(Sender: TObject);
begin
  Timer1.Enabled := false;
  Timer1.OnTimer := CloseApp;
  FHttpConnection := TMrInet.Create( Self as IStatus );
  FDownloader := TDruidDownloader.Create;
  try
    FDownloader.Path := FLocalPath;
    FDownloader.OnLoad := Self.OnLoad;
    FDownloader.Execute( Self );
    Timer1.Enabled := true;
  except on E:Exception do
  begin
    lblInfo.Caption := E.Message;
    lblInfo.Font.Color := clRed;
    lblInfo.WordWrap := true;
    Width := 400;
  end;
  end;
  FDownloader.Free;
  FHttpConnection.Free;
end;

procedure TfrmDruidDownload.CloseApp(Sender: TObject);
begin
  Close;
end;

procedure TfrmDruidDownload.Done;
begin
  ProgressBar1.Position := 100;
  SetInfo( 'Ferdig' );
end;

procedure TfrmDruidDownload.FindKey;
var
  regKey: TRegistry;
begin
  regKey := TRegistry.Create(KEY_READ);
  try
    regKey.RootKey := HKEY_CURRENT_USER;
    regKey.OpenKeyReadOnly('SOFTWARE\Emetra\Argus');
    FLocalPath := IncludeTrailingPathDelimiter(regKey.ReadString('AppDir')) + 'Lookup\';
    regKey.CloseKey;
  finally
    regKey.Free;
  end;
end;

procedure TfrmDruidDownload.FormShow(Sender: TObject);
begin
  FindKey;
  Timer1.OnTimer := DownloadNow;
  Timer1.Enabled := true;
end;

function TfrmDruidDownload.GetInfo: string;
begin
  Result := lblInfo.Caption;
end;

procedure TfrmDruidDownload.OnLoad(ADocument: IXmlDocument; const AUrl: string);
begin
  ADocument.LoadFromXml( FHttpConnection.GetString( AURL ) );
end;

procedure TfrmDruidDownload.SetInfo(const s: string);
begin
  lblInfo.Caption := s;
  lblInfo.Update;
end;

procedure TfrmDruidDownload.SetProgress(const APercent: integer);
begin
  ProgressBar1.Position := APercent;
end;

end.
