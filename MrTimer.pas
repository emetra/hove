unit MrTimer;

interface

uses
  SysUtils, Windows;

function GetHiTickCount: extended; { A high resolution counter in milliseconds }
function GetTimeStrSince( const ATickStart: extended; const ADecimals: integer = 3 ): string;
function GetNiceTimeFormat( const AMilliseconds: extended; const ADecimals: integer = 3; const AIncludeText: boolean = false; const AIncludeDays: boolean = false ): string; overload; {
  Summary:
    Formats a certain number of milliseconds into a nicer and more readable format }
function GetTimeElapsed( AStart: extended; const ADecimals: integer = 6 ): string; {
  Summary:
    Formats a certain number of milliseconds into a nicer and more readable format }
var
  lpTimer: TLargeInteger; { Holds last call to GetHiTickCount }
  TicksAtInit: Extended; { The hires system counter when this module was initialized }

type

TStopwatch = record
  strict private
    FElapsed: Int64;
    FRunning: Boolean;
    FStartTimeStamp: Int64;
    function GetElapsedMilliseconds: Int64;
    function GetElapsedTicks: Int64;
    function GetElapsedSeconds: Double;
    class procedure InitStopwatchType; static;
  public
    class function Create: TStopwatch; static;
    class function GetTimeStamp: Int64; static;
    procedure Reset;
    procedure Start;
    class function StartNew: TStopwatch; static;
    procedure Stop;
    property ElapsedMilliseconds: Int64 read GetElapsedMilliseconds;
    property ElapsedTicks: Int64 read GetElapsedTicks;
    property ElapsedSeconds: Double read GetElapsedSeconds;
    property IsRunning: Boolean read FRunning;
  public class var Frequency: Int64;
  public class var IsHighResolution: Boolean;
  end;

implementation

{$IFNDEF UNICODE}
uses
  MrD2009;
{$ENDIF}

function GetNiceTimeFormat( const AMilliseconds: extended; const ADecimals: integer = 3;
  const AIncludeText: boolean = false; const AIncludeDays: boolean = false ): string;
const
  SECOND = 1000;
  MINUTE = SECOND*60;
  HOUR = MINUTE*60;
  DAY = HOUR * 24;
var
  sDay,sHour,sMin,sSec: string;
  iMinute,
  iHour,
  iSecond,
  iDay: integer;
  Millisecs: int64;
begin
  if AMilliseconds < 100 then begin
    Result := Copy( Format( '%.'+IntToStr(ADecimals)+'f',[AMilliseconds] ), 1, ADecimals + 2 ) + 'ms';
    exit;
  end;
  sSec := '%.2d';
  if AIncludeText then begin
    sDay := '%d d ';
    sHour := '%d hr ';
    sMin := '%d min ';
  end
  else begin
    sDay := '%dd ';
    sHour := '%.2d'+FormatSettings.TimeSeparator;
    sMin := '%.2d'+FormatSettings.TimeSeparator;
  end;
  Millisecs := trunc( AMilliseconds );
  Result := EmptyStr;
  iDay := trunc( Millisecs / DAY );
  if iDay > 0 then
    Result := Result + Format( sDay, [ iDay ] );
  Millisecs := Millisecs mod DAY;
  if not AIncludeDays then
    Result := EmptyStr;
  iHour := trunc( Millisecs / HOUR );
  if (iHour > 0) or ( Result <>EmptyStr ) then
    Result := Result + Format( sHour,[iHour] );
  Millisecs := Millisecs mod HOUR;
  iMinute := trunc( Millisecs / MINUTE );
  if ( iMinute > 0 ) or ( Result <> EmptyStr ) then
    Result := Result + Format( sMin,[iMinute] );
  Millisecs := Millisecs mod MINUTE;
  if Result = EmptyStr then sSec := '%d';
  iSecond := trunc( Millisecs/ SECOND );
    Result := Result + Format( sSec, [iSecond] );
  Millisecs := Millisecs mod SECOND;
    Result := Result + Format( '.%.3d', [Millisecs] );
  if ( iMinute = 0 ) and ( iDay = 0 ) and (iHour = 0) then
    Result := StringReplace( Result, '00,','0,',[rfReplaceALl] ) + 's';
end;

function GetHiTickCount: extended;
var
  lpFrequency: TLargeInteger;
begin
  QueryPerformanceFrequency( lpFrequency );
  QueryPerformanceCounter( lpTimer );
  Result := ( 1000 * lpTimer / lpFrequency );
end;

function GetTimeStrSince( const ATickStart: extended; const ADecimals: integer = 3 ): string;
begin
  Result := GetNiceTimeFormat( GetHiTickCount - ATickStart, ADecimals );
end;

function GetTimeElapsed( AStart: extended; const ADecimals: integer = 6 ): string;
begin
  Result := GetNiceTimeFormat( GetHiTickCount - AStart, ADecimals );
end;

{$REGION 'TStopwatch'}

class function TStopwatch.Create: TStopwatch;
begin
  InitStopwatchType;
  Result.Reset;
end;

function TStopwatch.GetElapsedMilliseconds: Int64;
begin
  Result := GetElapsedTicks div (Frequency div 1000);
end;

function TStopwatch.GetElapsedSeconds: Double;
begin
  Result := ElapsedTicks / Frequency;
end;

function TStopwatch.GetElapsedTicks: Int64;
begin
  Result := FElapsed;
  if FRunning then
    Result := Result + GetTimeStamp - FStartTimeStamp;
end;

class function TStopwatch.GetTimeStamp: Int64;
begin
  if IsHighResolution then
    QueryPerformanceCounter(Result)
  else
    Result := GetTickCount * 1000;
end;

class procedure TStopwatch.InitStopwatchType;
begin
  if Frequency = 0 then
  begin
    IsHighResolution := QueryPerformanceFrequency(Frequency);
    if not IsHighResolution then
      Frequency := 1000;
  end;
end;

procedure TStopwatch.Reset;
begin
  FElapsed := 0;
  FRunning := False;
  FStartTimeStamp := 0;
end;

procedure TStopwatch.Start;
begin
  if not FRunning then
  begin
    FStartTimeStamp := GetTimeStamp;
    FRunning := True;
  end;
end;

class function TStopwatch.StartNew: TStopwatch;
begin
  InitStopwatchType;
  Result.Reset;
  Result.Start;
end;

procedure TStopwatch.Stop;
begin
  if FRunning then
  begin
    FElapsed := FElapsed + GetTimeStamp - FStartTimeStamp;
    FRunning := False;
  end;
end;

{$ENDREGION}

initialization
  TicksAtInit := ( Now - trunc( Now ) ) * 1000 * 60 * 60 * 24 - GetHiTickCount;
end.
