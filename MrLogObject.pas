{$M+}
{$IFDEF CODEHEALERANALYSIS}
  {$HINTS OFF}
{$ENDIF}
unit MrLogObject;

{$WARN SYMBOL_PLATFORM OFF}

interface

uses
  MrWin7,
  {$IFNDEF Unicode}
  MrD2009,
  {$ENDIF}
  {$IFDEF SmartInspect}
  SmartInspect, SiAuto,
  {$ENDIF}
  {$IFDEF DotNet}
  System.Text,
  {$ENDIF}
  MrTimer, MrBase,
  Classes, StrUtils, Controls, Dialogs, Forms, Graphics, StdCtrls, SysUtils, Windows, Registry, Inifiles;

type
{ Summary:
    The TLogObject class provides useful logging facilities for any application. }
  TLogObject = class( TInterfacedObject, ILogger )
  private
    FAppPath: string;
    FBtn: integer;
    FCalls: integer;
    FCancel: boolean;
    FCancelButton: boolean;
    FCompany: string;
    FDebugStrategy: TDebugStrategy;
    FDialogThreshold: TLogEntryType;
    FFilename: string;
    FIncludeTimeInfo: boolean; { Include time info on start of line }
    FIniFile: TIniFile;
    FLastMessage: string;
    FLogFile: TextFile;
    FLogLevel: TLogEntryType;
    FOnDialog: TOnDialogFunction;
    FRes: boolean;
    FStrings: TStrings;
    FUser: string;
    FYesNo: boolean;
    lpFrequency: TLargeInteger;
    class var InstanceCounter: integer;

    { Accessors }
    function Get_AppPath: string;
    function Get_Lines: TStrings;
    procedure Set_Threshold( ALogLevel: TLogLevel );
    function Get_Threshold: TLogLevel;
    procedure Set_ThresholdForDialog( ALogLevel: TLogLevel );
    function Get_ThresholdForDialog: TLogLevel;

    { Other members }
    function GetMarker( const ALevel: TLogLevel ): string;
    function WriteToFile: boolean;
    procedure CustomDrawColors( ACanvas: TCanvas; const AIndex: integer ); { Summary: Allows standard Brush and Font colors to be set for ACanvas, before painting Lines[AIndex] in the log. }
    procedure GetUserAndCompany;
    procedure Set_Lines(const Value: TStrings);
    {$IFDEF AppendLog}
    procedure SetFilename( const AFilename: string; const AAppend: boolean = true );
    {$ELSE}
    procedure SetFilename( const AFilename: string; const AAppend: boolean = false );
    {$ENDIF}
     {
      Summary:
        Allows the log to start writing to a logfile.  The logfile can be appended,
        or reset.
      Note:
        What is already in the memory (Lines) of this log is not written. To do this,
        first call SaveToFile, then SetFilename and AAppend=true. }

    procedure CustomDraw(Control: TWinControl; Index: Integer; Rect: TRect; State: TOwnerDrawState); {
      Summary:
        Use this method to paint a certain line in this log, similar to its on-screen appeareance,
        with colors etc. }
    {$IFNDEF SmartInspect }
    property Lines: TStrings read Get_Lines write Set_Lines; {
      Summary:
        The memory-resident part of this log object.
      Note:
        It is the responsibility of the creator to free and nil this TStrings object! }
    {$ENDIF}

    procedure LogString( const AName,AValue: string );
    function LogYesNo( const AMessage: string; const ALevel: TLogEntryType=ltMessage;
      const ACancel: boolean = false ): boolean; {
      Summary:
         Allows the log object to get a confirmation to the user, on a certain level.
         The default answer is always Yes if this message is below the dialog
         threshold, and there is no Cancel button as default. }

    procedure Event( AMessage: string; const ALogType: TLogEntryType = ltInfo ); overload; {
      Add a message to the log }

    procedure Event( const AMessage: string; const Args: array of const; const ALogType: TLogEntryType = ltInfo ); overload; {
      Add a formatted message to the log }

    property ThresholdForDialog: TLogEntryType read FDialogThreshold write FDialogThreshold; {
      Summary: At what logging level should the user be bothered with a dialog box? }

    function SetPriority( const APriorityClass: dword ): boolean; {
      Summary:
        Allows the priority of this application to be set, using Windows API }

    procedure SetUserFile( AIniFile: TInifile ); {
      Summary:
        The User file stores information about messages that are suppressed (user clicked Ignore) }

    procedure ShowMessage( const AMessage: string; const ALevel: TLogEntryType = ltMessage; const AMaxTimes: integer = maxint ); {
      Summary:
        Allows AMessage to be shown, but limited to AMaxTimes number of times,
        or to let the user click Ignore never to see this message again,
        provided that FIniFile is assigned.  The message is not shown if it is below the
        threshold, but the counter is incremented.
      Note:
        This method uses a simple checksum as a digest of the message.  Usually the chance
        of collision is small, as the number of messages in an application is not that great.  }

    procedure SaveFatalExit;
    function StripNewlines( const AMsg: string ): string;
  public
    constructor Create;
    destructor Destroy; override;
  published
    property AppPath: string read Get_AppPath;
    property Calls: integer read FCalls;
    property Filename: string read FFilename; { Summary: The current log filename }
    property RegisteredCompany: string read FCompany; { The company this copy of the OS is registered to }
    property RegisteredUser: string read FUser; { The user this copy of the OS is registered to }
  end;

implementation

const
  LOG_TYPE = '�';
  LOG_DEBUG_LINE = LOG_TYPE + 'debug';
  LOG_INFO_LINE = LOG_TYPE + 'info ';
  LOG_MESSAGE_LINE = LOG_TYPE + 'messg';
  LOG_WARNING_LINE = LOG_TYPE + 'warn ';
  LOG_ERROR_LINE = LOG_TYPE + 'error';
  LOG_CRITICAL_LINE = LOG_TYPE + 'crit ';

  LOG_LINE_MARKERS: array [TLogLevel] of string = (LOG_DEBUG_LINE, LOG_INFO_LINE, LOG_MESSAGE_LINE, LOG_WARNING_LINE,
    LOG_ERROR_LINE, LOG_CRITICAL_LINE);

const
  BELOW_NORMAL_PRIORITY_CLASS = $00004000;

  CLASS_NAME = 'TLogObject';
  LOG_TIME_LINE = LOG_TYPE + 'ptime';
  LOG_STR = '%s %.6d %s~';
  MAX_LINE = 4999;
  BRACE_FIRST = 10;
  BRACE_SECOND = 15;

function GetLogFileName( const AExtraFileId: string = '' ): string;
const
  LOG_EXT = '.LOG';
var
  strLogFile: string;
begin
  strLogFile := ExtractFileName( Application.ExeName );
  strLogFile := ChangeFileExt( strLogFile, '.LOG' );
  if AExtraFileId <> EmptyStr then
    strLogFile := StringReplace( strLogFile, LOG_EXT, '-' + AExtraFileId + LOG_EXT, [] );
  Result := ExtractFilePath( Application.ExeName ) + 'LOGS\' + strLogFile;
end;

function TLogObject.WriteToFile: boolean;
begin
  Result := FFilename <> EmptyStr;
end;

constructor TLogObject.Create;
begin
  inherited;
  Assert( InstanceCounter = 0 );
  inc( InstanceCounter );
  if IsConsole then
    FOnDialog := nil
  else
    FOnDialog := Dialogs.MessageDlg;
  FCancelButton := false;
  FIniFile := nil;
  FIncludeTimeInfo := true;
  FDebugStrategy := dbgNone;
  QueryPerformanceFrequency( lpFrequency );
  FLogLevel := ltInfo;
  FDialogThreshold := ltMessage;
  {$IFNDEF SmartInspect}
  FStrings := nil;
  {$ENDIF}
  FLastMessage := 'NOTHING';
  GetUserAndCompany;
end;

procedure TLogObject.Event( AMessage: string; const ALogType: TLogEntryType );
var
  OrigMsg: string;
  LogStr: string;
  strType: string;
  ShowMsg: string;
  TimerStr: string;
  mt: TMsgDlgType;
  crSaved: TCursor;
begin
  inc( FCalls );
  if ( ALogType < FLogLevel ) and ( FDebugStrategy = dbgNone ) then exit;
  if ( AMessage = FLastMessage ) and ( ALogType < ThresholdForDialog ) then exit
  else FLastMessage := AMessage;
  OrigMsg := AMessage;
  AMessage := StripNewlines( AMessage );
  if AMessage = EmptyStr then
  begin
    TimerStr := FormatDateTime( 'hh:mm:ss', Now );
    AMessage := TimerStr{$IFNDEF DotNet} + ' thread: ' + IntToStr( MainThreadID ){$ENDIF};
    strType := LOG_TIME_LINE;
  end
  else
    strType := LOG_LINE_MARKERS[ALogType];
  if FIncludeTimeInfo then
  begin
    LogStr := Format( LOG_STR, [ strType, FCalls, AMessage ] );
    TimerStr := Format( '%s ', [ GetNiceTimeFormat( GetHiTickCount + TicksAtInit )] )
  end
  else begin
    LogStr := Format( '%s %.6d %s', [ strType, FCalls,AMessage ] );
    TimerStr := '';
  end;
  if IsConsole then begin
    if WriteToFile then
      WriteLn( FLogFile, TimerStr + LogStr )
    else
      WriteLn( TimerStr + LogStr )
  end
  else begin
    { We want to store the log data }
    {$IFNDEF SmartInspect }
    if Assigned( FStrings ) then begin
      if( FStrings.Count >= MAX_LINE ) then FStrings.Delete( 0 );
      FStrings.Add( TimerStr + LogStr );
    end;
    {$ELSE}
    case ALogType of
      ltDebug: SiMain.LogColored( lvDebug, clWebAliceBlue, LogStr );
      ltInfo: SiMain.LogVerbose( LogStr );
      ltMessage: SiMain.LogColored( lvMessage, clWebLawnGreen, LogStr );
      ltWarning: SiMain.LogColored( lvWarning, clYellow, LogStr );
      ltError: SiMain.LogColored( lvError, clWebPink, LogStr );
      ltCritical: SiMain.LogColored( lvFatal, clWebHotPink, LogStr );
    end;
    {$ENDIF}
    if WriteToFile then try
      WriteLn( FLogFile, TimerStr + LogStr );
    except on Exception do
      FFilename := EmptyStr;
    end;
  end;

  { Dialog box if needed }
  ShowMsg := StringReplace( OrigMsg, '\n', CRLF, [rfReplaceAll]);
  if ( ALogType >= FDialogThreshold )
  or ( ( ALogType = ltDebug) and ( FDebugStrategy = dbgShowDialog ) ) then begin
    // OutputDebugString( pChar( LogStr ) );
    case ALogType of
      ltWarning: mt := mtWarning;
      ltError..ltCritical: mt := mtError;
    else
      mt := mtInformation;
    end;
    crSaved := Screen.Cursor;
    Screen.Cursor := crDefault;
    if Assigned( FOnDialog ) then begin
      if Assigned( Progress ) then
        Progress.Done;
      if FYesNo then begin
        if mt = mtInformation then mt := mtConfirmation;
        if FCancel then FBtn := FOnDialog( ShowMsg, mt, [mbYes,mbNo,mbCancel], 0 )
        else FBtn :=  FOnDialog( ShowMsg, mt, [mbYes,mbNo], 0 );
        Screen.Cursor := crSaved;
        FRes := ( FBtn = mrYes );
        if FBtn = mrCancel then begin
          FYesNo := false;
          FCancel := false;
          raise EAbort.Create( 'CanceledByUser' );
        end;
      end
      else if FCancelButton then
        FBtn := FOnDialog( ShowMsg, mt, [mbOk,mbIgnore], 0 )
      else
        FOnDialog(  ShowMsg, mt, [mbOk], 0 );
    end;
    Screen.Cursor := crSaved;
  end;
end;


procedure TLogObject.Event( const AMessage: string; const Args: array of const; const ALogType: TLogEntryType );
begin
  if ( ALogType < FLogLevel ) and ( FDebugStrategy = dbgNone ) then
    exit
  else
    Event( Format( AMessage, Args ), ALogType );
end;

procedure TLogObject.CustomDrawColors( ACanvas: TCanvas; const AIndex: integer );
var
  iBrushPos: integer;
  iFontPos: integer;
begin
  {$IFNDEF SmartInspect}
  if FStrings = nil then exit;
  with ACanvas do begin
    Font.Style := [];
    Brush.Color := clWhite;
    iBrushPos := PosEx( LOG_TYPE, FStrings[AIndex], BRACE_FIRST );
    if iBrushPos >= BRACE_FIRST then
    case FStrings[AIndex][iBrushPos+1] of
      'd': Brush.Color := clWebBisque;
      't': Brush.Color := clWebAliceBlue;
      'i': Brush.Color := clWhite;
      'm': Brush.Color := clWebGreenYellow;
      'w': Brush.Color := clYellow;
      'e': Brush.Color := clWebOrange;
      'p': begin Font.Color := clWhite; Brush.Color := clSilver; end;
      'x': begin Font.Color := clWhite; Brush.Color := clWebTomato; end;
      'c': begin Font.Color := clWhite;  Brush.Color := clWebCrimson; end;
    end;
    Font.Color := clBlack;
    iFontPos := PosEx( TAG_FONT_COLOR, FStrings[AIndex], BRACE_SECOND );
    if iFontPos>=BRACE_SECOND then begin
      Font.Style := [fsBold];
      case FStrings[AIndex][iFontPos+2] of
        'y': Brush.Color := clYellow;
        'r': Font.Color := clRed;
        'g': Font.Color := clGreen;
        'b': Font.Color := clBlue;
        'n': Font.Color := clNavy;
        'o': begin Brush.Color := clOlive; Font.Color := clWhite end;
        'm': Font.Color := clMaroon;
        'l': Font.Color := clPurple;
      else
        Font.Style := [];
      end;
    end;
  end;
  {$ENDIF}
end;


procedure TLogObject.CustomDraw(Control: TWinControl; Index: Integer;
  Rect: TRect; State: TOwnerDrawState);
begin
  {$IFNDEF SmartInspect}
  if FStrings = nil then exit;
    with Control as TListBox do begin
      if not ( odSelected in State ) then begin
        CustomDrawColors( Canvas, Index );
        Canvas.FillRect( Rect );
      end
      else
        Canvas.FillRect( Rect );
      {$IFDEF DotNet}
      DrawText( Canvas.Handle, FStrings[Index], Length( FStrings[Index] ), Rect, DT_VCENTER + DT_SINGLELINE  );
      {$ELSE}
      DrawText( Canvas.Handle, pChar( FStrings[Index] ), Length( FStrings[Index] ), Rect, DT_VCENTER + DT_SINGLELINE  );
      {$ENDIF}
    end;
  {$ENDIF}
end;

procedure TLogObject.LogString(const AName, AValue: string);
begin
  Event( AName + '=' + AValue );
end;

function TLogObject.LogYesNo( const AMessage: string; const ALevel: TLogEntryType =ltMessage; const ACancel: boolean = false ): boolean;
begin
  FRes := true;
  FCancel := ACancel;
  FYesNo := true;
  Event( AMessage, ALevel );
  FYesNo := false;
  Result := FRes;
end;

function TLogObject.GetMarker(const ALevel: TLogLevel): string;
begin
  Result := LOG_LINE_MARKERS[ALevel];
end;

procedure TLogObject.GetUserAndCompany;
const
  Reg95 =   '\SOFTWARE\Microsoft\Windows\CurrentVersion';
  RegNT =   '\SOFTWARE\Microsoft\Windows NT\CurrentVersion';
  OwnerKey = 'RegisteredOwner';
  OrganizationKey = 'RegisteredOrganization';
var
  RegKey: TRegistry;
begin
  FUser := EmptyStr;
  FCompany := EmptyStr;
  RegKey := TRegistry.Create;
  with RegKey do begin
    RegKey.RootKey := HKEY_LOCAL_MACHINE;
    try
      OpenKeyReadOnly( Reg95 );
      FCompany := ReadString( OrganizationKey );
      FUser := ReadString( OwnerKey );
      CloseKey;
    except on E:Exception do
    end;
    if FCompany=EmptyStr then try
      OpenKeyReadOnly( RegNT );
      FCompany := ReadString( OrganizationKey );
      FUser := ReadString( OwnerKey );
      CloseKey;
    except on E:Exception do
    end;
    Free;
  end;
end;

function TLogObject.Get_AppPath: string;
begin
  if FAppPath = EmptyStr then
    FAppPath := ExtractFilePath( Application.ExeName );
  Result := FAppPath;
end;

function TLogObject.Get_Lines: TStrings;
begin
  Result := FStrings;
end;

function TLogObject.StripNewlines(const AMsg: string): string;
begin
  Result := StringReplace( AMsg, CRLF, ' ', [rfReplaceAll] );
  Result := StringReplace( Result, '\n',' ', [rfReplaceAll] );
end;

procedure TLogObject.SetFilename(const AFilename: string; const AAppend: boolean );
const
  PROC_NAME = CLASS_NAME + '.SetLogFile: ';
var
  bSuccessfulOpen: boolean;
  n: integer;
begin
  if ( AFilename=EmptyStr ) then begin
    if WriteToFile then begin
      Event( PROC_NAME + 'Closing logfile.', ltInfo );
      Close( FLogFile );
    end;
    FFilename := EmptyStr;
  end
  else
  try
    if not ForceDirectories( ExtractFilePath( AFilename ) ) then
      Event( PROC_NAME + 'Could not create %s' + TAG_YELLOW, [AFilename] )
    else begin
      FFilename := AFilename;
      bSuccessfulOpen := false;
      for n:=1 to 64 do try
        AssignFile( FLogFile, FFilename );
        if AAppend and FileExists( FFilename ) then
        begin
          Append( FLogFile );
          WriteLn( FLogFile, DupeString( '=', 80 ) );
          WriteLn( FLogFile, 'New Session' );
          WriteLn( FLogFile, DupeString( '-', 80 ) );
        end
        else
          Rewrite( FLogFile );
        bSuccessfulOpen := true;
        break;
      except on E:Exception do
        FFilename := ChangeFileExt( FFilename, Format( '.%.3d', [n] ) );
      end;
      if not bSuccessfulOpen then
        raise Exception.Create( 'No available filenames' )
      else
        Event( PROC_NAME + FFilename, ltInfo );
      Event( PROC_NAME + 'LCID=%d ShortDateFormat=%s', [GetUserDefaultLCID,FormatSettings.ShortDateFormat], ltInfo );
    end;
  except on E:Exception do
    begin
      FFilename := EmptyStr;
      Event( PROC_NAME + E.Message + TAG_RED, ltInfo );
    end;
  end;
end;

destructor TLogObject.Destroy;
begin
  dec( InstanceCounter );
  SetFilename( EmptyStr );
  inherited;
end;

procedure TLogObject.SetUserFile(AIniFile: TInifile);
begin
  FIniFile := AIniFile;
end;

procedure TLogObject.ShowMessage(const AMessage: string;
  const ALevel: TLogEntryType = ltMessage; const AMaxTimes: integer = maxint );
const
  SECTION_MESSAGE = 'Message';
var
  iHash: integer;
  iCount: integer;
  strKey: string;
begin
  FCancelButton := true;
  iHash := GetMessageDigest( AMessage );
  iCount := 0;
  strKey := Format( 'MSG_%.8x',[iHash] );
  if Assigned( FIniFile ) then
    iCount := FIniFile.ReadInteger( SECTION_MESSAGE, strKey, 0 );
  if iCount < AMaxTimes then begin
    Event( AMessage + '\n\n' + Format( LOG_IGNORE, [BTN_IGNORE] ), ALevel );
    if ( iCount < maxint ) and Assigned( FIniFile ) then
    begin
      inc( iCount );
      if FBtn = mrIgnore then iCount := AMaxTimes;
      FIniFile.WriteInteger( SECTION_MESSAGE, strKey, iCount );
    end;
  end;
  FCancelButton := false;
end;

procedure TLogObject.SaveFatalExit;
begin
  {$IFNDEF SmartInspect}
  if Lines <> nil then
    Lines.SaveToFile( ChangeFileExt( Application.ExeName, '.ERR' ) );
  {$ENDIF}
end;

function TLogObject.SetPriority( const APriorityClass: dword ): boolean;
const
  PROC_NAME = CLASS_NAME + '.SetPriority: ';
begin
  { Set priority }
  Result := SetPriorityClass( GetCurrentProcess(), APriorityClass );
  if Result then
    Event( PROC_NAME + 'SetPriority=%d',[APriorityClass], ltInfo )
  else
    Event( PROC_NAME + 'SetPriority failed {y}', ltInfo );
end;


procedure TLogObject.Set_Threshold( ALogLevel: TLogLevel );
begin
  FLogLevel := ALogLevel;
end;

function TLogObject.Get_Threshold: TLogLevel;
begin
  Result := FLogLevel;
end;

procedure TLogObject.Set_ThresholdForDialog( ALogLevel: TLogLevel );
begin
    FDialogThreshold := ALogLevel;
end;

function TLogObject.Get_ThresholdForDialog: TLogLevel;
begin
  Result := FDialogThreshold;
end;

procedure TLogObject.Set_Lines(const Value: TStrings);
begin
  FStrings := Value;
end;

initialization
  LOG := TLogObject.Create;
  LOG.SetFilename( GetLogFileName( GetWindowsUserName + '@' +  GetWindowsComputerName )  );
finalization
  LOG.SetFilename( EmptyStr );
end.
