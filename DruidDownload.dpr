program DruidDownload;

uses
  Forms,
  Main.Druid.Download in 'Main.Druid.Download.pas' {frmDruidDownload};

{$R *.res}

begin
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  Application.CreateForm(TfrmDruidDownload, frmDruidDownload);
  Application.Run;
end.
