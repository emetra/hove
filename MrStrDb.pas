unit MrStrDb;

interface

uses

  // Std modules
  MrClasses,
  SysUtils, Classes, Dialogs, Windows;

const
  MinIndex = 0;
  MaxIndex = 15;
  MaxField = 7;

  FDivider = ' ' + #9 + ' ';
  IDX_TAG = '�P=';
  TAG_LEN = 3;

type
  TStrDb = class(TStringList)
  private
    FEOF: boolean;
    FBOF: boolean;
    FFieldCount: integer;
    FStartRange: integer;
    FEndRange: integer;
    FFieldNames: array[0..MaxField] of string;
    FIndexFieldCount: array[MinIndex..MaxIndex] of integer;
    FIndexFields: array[MinIndex..MaxIndex] of array[0..MaxField] of integer;
    FIndexPos: array[MinIndex..MaxIndex] of integer;
    FIndexNames: array[MinIndex..MaxIndex] of string;
    FIndexList: array[MinIndex..MaxIndex] of TStringList;
    FCurrIndex: integer;
    FIndexCount: integer;
    FTag: integer;
    FName: string;
    FFilename: string;
 { Private declarations }
  protected
 { Protected declarations }
    procedure PositionFromIndexEntry(IndexEntry: integer);
    procedure RecordAsFields(Line: string; var FieldValues: array of string);
    function FieldsAsRecord(FieldValues: array of string): string;
    function GetIndexName: string;
    procedure SetIndexName(Value: string);
    function GetIndexItem: integer;
    procedure SetIndexItem(Value: integer);
  public
 { Public declarations }
    ItemIndex: integer;
    LastError: string;
    constructor Create(Name: string; FieldNames: array of string);
    procedure SetString(index: integer; Value: string);
    function GetString(index: integer): string;
    procedure SetFieldNames(FieldNames: array of string);
    procedure GetFieldNames(var FieldNames: array of string);
    function IsEmpty: boolean;
    function EOF: boolean;
    function BOF: boolean;
    function Last: boolean;
    function First: boolean;
    function Prior: boolean;
    function Next: boolean;
    function CreateIndex(FieldsNos: array of integer; IndexName: string): boolean;
    function FindKey(FieldValues: array of string): boolean;
    procedure FindNearest(FieldValues: array of string);
    procedure AssignIndexToList(IndexName: string; Strings: TStrings);
    procedure SetRange(FromValues, ToValues: array of string);
    function RecordCount: integer;
    procedure CancelRange;
    property Filename: string read FFilename;
    function GetFieldNo(FieldName: string): integer;
    function Load(path: string): boolean;
    function Save(path: string): boolean;
    function GetSubset(SearchFor: string; ReturnFields: array of integer; const AColDelimiter: string): TStringList;
  published
 { Published declarations }
    property IndexItem: integer read GetIndexItem write SetIndexItem;
    property IndexName: string read GetIndexName write SetIndexName;
    property Tag: integer read FTag write FTag;
    property Name: string read FName;
  end;

function CreateRow(Fields: array of string): string;


implementation


function CreateRow(Fields: array of string): string;
var
  ResultStr: string;
  n: integer;
begin
  n := 0;
  ResultStr := '';
  while n <= high(Fields) do begin
    ResultStr := ResultStr + Fields[n];
    if n < high(Fields) then ResultStr := ResultStr + FDivider;
    inc(n);
  end;
  Result := ResultStr;
end;

{
procedure Register;
begin
  RegisterComponents('MAGNUM', [TStrDb]);
end;
}



constructor TStrDb.Create(Name: string; FieldNames: array of string);
var
  n: integer;
begin
  inherited Create;
  FFieldCount := 0;
  FIndexCount := 1;
  FEOF := true;
  FBOF := true;
  FCurrIndex := 0;
  FIndexList[0] := Self;
  FIndexNames[0] := '';
  for n := 0 to MaxField do
    FIndexFields[0][n] := n;
  FName := Name;
  SetFieldNames(FieldNames);
end;


function TStrDb.IsEmpty: boolean;
begin
  Result := false;
  if (Self.Count = 0) or (FStartRange > FEndRange) or (FEndRange < 0) then begin
  // The entire table is empty
    FEOF := true;
    FBOF := true;
    Result := true;
  end;
end;


function TStrDB.Save(path: string): boolean;
begin
  try
    Self.SaveToFile(path + FName + '.tsv');
    Result := true;
  except on Exception do
      Result := false;
  end;
end;


function TStrDb.Load(path: string): boolean;
var
  TempStr: string;
  DivPos: integer;
begin
  Result := false;
  path := IncludeTrailingPathDelimiter(path);
  FFilename := path + FName + '.tsv';
  if FileExists( FFileName ) then
  try
    LoadFromFile(FFilename);
    FFieldCount := 1;
    if Self.Count > 0 then TempStr := Self[0];
    DivPos := Pos(FDivider, TempStr);
    while DivPos <> 0 do begin
      System.Delete(TempStr, DivPos, Length(FDivider));
      DivPos := Pos(FDivider, TempStr);
      inc(FFieldCount);
    end;
    FStartRange := 0;
    FEndRange := Self.Count - 1;
    Result := true;
  except on E: Exception do
      LastError := E.Message;
  end;
end;


procedure TStrDb.SetFieldNames(FieldNames: array of string);
var
  n: integer;
begin
  n := 0;
  while (n <= MaxField) and (n <= High(FieldNames)) do begin
    FFieldNames[n] := FieldNames[n];
    inc(n);
  end;
end;


procedure TStrDb.GetFieldNames(var FieldNames: array of string);
var
  n: integer;
begin
  n := 0;
  while (n <= MaxField) and (n <= High(FieldNames)) do begin
    FieldNames[n] := FFieldNames[n];
    inc(n);
  end;
end;


function TStrDb.GetFieldNo(FieldName: string): integer;
var
  n: integer;
begin
  Result := -1;
  n := 0;
  while (n <= MaxField) do
    if Uppercase(FieldName) = UpperCase(FFieldNames[n]) then
    begin
      Result := n;
      exit;
    end
    else
      inc(n);
end;


function TStrDb.GetIndexItem: integer;
begin
  if FCurrIndex <> -1 then Result := FIndexPos[FCurrIndex]
  else Result := -1;
end;


procedure TStrDb.SetIndexItem(Value: integer);
begin
  if FCurrIndex <> -1 then FIndexPos[FCurrIndex] := Value;
end;


function TStrDb.RecordCount: integer;
begin
  if FStartRange <> -1 then Result := FEndRange - FStartRange + 1
  else Result := 0;
end;


procedure TStrDb.AssignIndexToList(IndexName: string; Strings: TStrings);
begin
  SetIndexName(IndexName);
  Strings.Assign(FIndexList[FCurrIndex]);
end;

function CompareRecords(Values1, Values2: array of string; fieldcount: integer): integer;
var
  CompResult: integer;
  fieldno: integer;
begin
  fieldno := 0;
  CompResult := 0;
  while (fieldno < fieldcount) and (CompResult = 0) do begin
    CompResult := AnsiCompareText(Values1[fieldno], Values2[fieldno]);
    inc(fieldno);
  end;
  Result := CompResult;
end;

procedure TStrDb.SetRange(FromValues, ToValues: array of string);
var
  FromRecord: string;
  ToRecord: string;
  RangeEndValues: array[0..MaxField] of string;
  CmpResult: integer;
begin
  if FCurrIndex = -1 then begin
    LastError := 'TStrDb.SetRange, NoIndexSelected';
    raise Exception.Create(LastError);
  end;

 //------------------------------------------------------------------------//
 // OBS: Problem:
 // Hvis det siste elementet finnes i ett eksemplar g�r alt greit
 // Hvis elementet ikke finnes, blir range ett hakk for lang.
 // Hvis det finnes flere, finnes bare den f�rste
 //------------------------------------------------------------------------//

 // Sett inn pr�veelement basert p� FromValues for � finne starten p� omr�det
  FromRecord := FieldsAsRecord(FromValues);
  FIndexList[FCurrIndex].Find(FromRecord, FStartRange);

 // Sett inn pr�veelement basert p� ToValues for � finne slutten p� omr�det
  ToRecord := FieldsAsRecord(ToValues);
  FIndexList[FCurrIndex].Find(ToRecord, FEndRange);

 //------------------------------------------------------------------------//
 // Dette m� til:
 // Sjekk om current er inkludert, hvis ikke krympes range
 // Hvis current er inkludert, se om neste er et duplikat
 //------------------------------------------------------------------------//
  if FEndRange < Self.Count then begin
    RecordAsFields(FIndexList[FCurrIndex][FEndRange], RangeEndValues);
    CmpResult := CompareRecords(RangeEndValues, ToValues, high(ToValues) + 1);

    while (CmpResult = 0) and (FEndRange + 1 < Self.Count) do begin
   // Is included, look for more duplicates
      inc(FEndRange);
      RecordAsFields(FIndexList[FCurrIndex][FEndRange], RangeEndValues);
      CmpResult := CompareRecords(RangeEndValues, ToValues, high(ToValues) + 1);
    end;
  end
  else
    CmpResult := 1;

  if CmpResult > 0 then begin
    dec(FEndRange);
  // Flytt et hakk tilbake hvis CurrRecord > ToRecord
  end;

  if FStartRange > FEndRange then begin
    FStartRange := -1;
    FEndRange := -2;
  end;
  if FStartRange < Self.Count then
    ItemIndex := FStartRange
  else
    ItemIndex := -1;
end;


procedure TStrDb.CancelRange;
begin
  if Self.Count > 0 then begin
    FStartRange := 0;
    FEndRange := Self.Count - 1;
    ItemIndex := 0;
  end
  else begin
    FStartRange := -1;
    FEndRange := -2;
    ItemIndex := -1;
  end;
end;


function TStrDb.EOF: boolean;
begin
  Result := FEOF;
end;


function TStrDb.BOF: boolean;
begin
  Result := FBOF;
end;


function TStrDb.First: boolean;
begin
  Result := false;
  if IsEmpty then exit;
 // The range is not empty
  PositionFromIndexEntry(FStartRange);
  FEOF := false;
  FBOF := true;
  Result := true;
end;


function TStrDb.Last: boolean;
begin
  Result := false;
  if IsEmpty then exit;
 // The range is not empty
  PositionFromIndexEntry(FEndRange);
  FEOF := true;
  FBOF := false;
  Result := true;
end;


function TStrDb.Prior: boolean;
begin
  Result := false;
  FEOF := false;
  if IsEmpty then exit;
  if FIndexPos[FCurrIndex] > FStartRange then begin
    PositionFromIndexEntry(FIndexPos[FCurrIndex] - 1);
    Result := true
  end
  else
    FBOF := true;
end;


function TStrDb.Next: boolean;
begin
  Result := false;
  FBOF := false;
  if IsEmpty then exit;
  if FIndexPos[FCurrIndex] < FEndRange then begin
    PositionFromIndexEntry(FIndexPos[FCurrIndex] + 1);
    Result := true;
  end
  else
    FEOF := true;
end;


procedure TStrDb.PositionFromIndexEntry(IndexEntry: integer);
var
  Loc: integer;
begin
  if IsEmpty then exit;
  if IndexEntry >= Self.Count then begin
    FEOF := true;
    exit;
  end;
  if IndexEntry < 0 then begin
    FBOF := true;
    exit;
  end;
  if FCurrIndex = 0 then
    ItemIndex := IndexEntry
  else
  begin
    Loc := Pos(IDX_TAG, FIndexList[FCurrIndex][IndexEntry]);
    ItemIndex := StrToInt(Copy(FIndexList[FCurrIndex][IndexEntry], Loc + TAG_LEN, 12));
  end;
  FIndexPos[FCurrIndex] := IndexEntry;
end;


function TStrDb.GetIndexName: string;
begin
  if FCurrIndex = -1 then Result := ''
  else Result := FIndexNames[FCurrIndex];
end;


procedure TStrDb.SetIndexName(Value: string);
begin
  FCurrIndex := 0;
  CancelRange;
  if Value = '' then exit;
  while FCurrIndex < FIndexCount do begin
    if FIndexNames[FCurrIndex] = Value then exit;
    inc(FCurrIndex);
  end;
  FCurrIndex := -1;
  raise Exception.Create(#10 + 'StrDB.SetIndexName(' + FName + ')'#10 + 'No such index name:(' + value + ')'#10);
end;


procedure TStrDb.RecordAsFields(Line: string; var FieldValues: array of string);
type
  TParsePos = (moInField, moPossiblyInDivider);
var
  FieldNo: integer;
  SplitPos: integer;
begin
  if IsEmpty then exit;
  FieldNo := 0;
  SplitPos := Pos(FDivider, Line);
  while (SplitPos > 0) and (FieldNo < FFieldCount) do begin
    FieldValues[FieldNo] := Copy(Line, 1, SplitPos - 1);
    System.Delete(Line, 1, SplitPos + Length(FDivider) - 1);
    SplitPos := Pos(FDivider, Line);
    inc(FieldNo);
  end;
  FieldValues[FieldNo] := Line;
end;


function TStrDb.GetString(index: integer): string;
var
  FieldValues: array[0..MaxField] of string;
begin
  Result := '';
  if IsEmpty then exit;
  RecordAsFields(Self[ItemIndex], FieldValues);
  if index < FFieldCount then Result := FieldValues[index]
end;


procedure TStrDb.SetString(index: integer; Value: string);
var
  FieldValues: array[0..MaxField] of string;
begin
  if IsEmpty then exit;
  RecordAsFields(Self[ItemIndex], FieldValues);
  if index < FFieldCount then begin
    FieldValues[index] := Value;
    Self[ItemIndex] := FieldsAsRecord(FieldValues);
  end;
end;


function TStrDb.FieldsAsRecord(FieldValues: array of string): string;
var
  FieldNo: integer;
begin
  Result := '';
  FieldNo := 0;
  while FieldNo < FFieldCount do begin
    if FieldNo <= high(FieldValues) then
      Result := Result + FieldValues[FieldNo];
    if FieldNo <> high(FieldValues) then
      Result := Result + FDivider;
    inc(FieldNo);
  end;
end;

function TStrDb.GetSubset(SearchFor: string; ReturnFields: array of integer; const AColDelimiter: string): TStringList;
var
  Line: string;
  m, n: integer;
begin
  SearchFor := AnsiLowercase(SearchFor);
  Result := TStringList.Create;
  Result.Sorted := true;
  Result.Duplicates := dupIgnore;
  n := 0;
  while n < Self.Count do begin
    if Pos(SearchFor, AnsiLowercase(Self[n])) <> 0 then
    begin
      Tokens.Prepare( Self[n], FDivider );
      Line := '';
      for m := low(ReturnFields) to high(ReturnFields) do
        Line := Line + Trim( Tokens[ReturnFields[m]-1] ) + AColDelimiter;
      Result.Add(Copy(Line, 1, Length(Line) - Length(AColDelimiter)));
   // Result.Add( Line );
    end;
    inc(n);
  end;
end;


function TStrDb.FindKey(FieldValues: array of string): boolean;
var
  TempRecord: string;
  InsertPos: integer;
  InsertPosValues: array[0..MaxField] of string;
  FieldNo: integer;
begin
  Result := false;
  if IsEmpty then exit;
 // Create record to look for using supplied order
 // Can not use Record from Fields, because it must be index-specific
  TempRecord := FieldValues[0];
  FieldNo := 1;
  while FieldNo <= high(FieldValues) do begin
    TempRecord := TempRecord + FDivider + FieldValues[FieldNo];
    inc(FieldNo);
  end;

 // Find insert position, and look forward from position
  FIndexList[FCurrIndex].Find(TempRecord, InsertPos);
  if InsertPos = Self.Count then exit; // Not found in list

 // Compare records in normal order
  RecordAsFields(FIndexList[FCurrIndex][InsertPos], InsertPosValues);
  if CompareRecords(FieldValues, InsertPosValues, high(FieldValues) + 1) = 0 then begin
  // Position correct
    PositionFromIndexEntry(InsertPos);
    Result := true;
  end
  else begin
    Result := false;
  end;
end;


procedure TStrDb.FindNearest(FieldValues: array of string);
var
  MyString: string;
  TempRecord: string;
  InsertPos: integer;
  FieldNo: integer;
begin
  if IsEmpty then exit;
 // Create record to look for using supplied order
 // Can not use Record from Fields, because it must be index-specific
  TempRecord := FieldValues[0];
  FieldNo := 1;
  while FieldNo <= high(FieldValues) do begin
    TempRecord := TempRecord + FDivider + FieldValues[FieldNo];
    inc(FieldNo);
  end;

 // Put it in index, and look forward from position
  FIndexList[FCurrIndex].Find(TempRecord, InsertPos);
  if InsertPos < FIndexList[FCurrIndex].Count then begin
    MyString := FIndexList[FCurrIndex][InsertPos];
    PositionFromIndexEntry(InsertPos);
  end;

end;


function TStrDb.CreateIndex(FieldsNos: array of integer; IndexName: string): boolean;
var
  FieldValues: array[0..MaxField] of string;
  TempRecord: string;
  LineNo: integer;
  FieldNo: integer;
begin
  Result := false;
  if FIndexCount > MaxIndex then exit;
 // Set up information about index
  FIndexList[FIndexCount] := TStringList.Create;
  FIndexList[FIndexCount].Sorted := true;
  FIndexNames[FIndexCount] := IndexName;
  for FieldNo := 0 to high(FieldsNos) do
    FIndexFields[FIndexCount][FieldNo] := FieldsNos[FieldNo];
  FIndexFieldCount[FIndexCount] := high(FieldsNos) + 1;
 // Start making index
  LineNo := 0;
  while LineNo < Self.Count do begin
    RecordAsFields(Self[LineNo], FieldValues);
    FieldNo := 0;
    TempRecord := '';
  // Add all selected fields to index
    while FieldNo < FIndexFieldCount[FIndexCount] do begin
      TempRecord := TempRecord + FieldValues[FieldsNos[FieldNo]] + FDivider;
      inc(FieldNo);
    end;
  // Add number of original line for later lookup
    TempRecord := TempRecord + Format(IDX_TAG + '%d', [LineNo]);
    FIndexList[FIndexCount].Add(TempRecord);
    inc(LineNo);
  end;
  FStartRange := 0;
  FEndRange := FIndexList[FIndexCount].Count;
 // Sort the index
  FCurrIndex := FIndexCount;
  First;
  inc(FIndexCount);
  Result := true;
end;


end.

