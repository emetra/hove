unit FLINKgen;

interface

uses
  Classes, Math, SysUtils;

type
  TResultType = ( rtSection, rtUnion );
  TSortBy = ( sbName, sbATC, sbRel, sbDesc );
  TATCStyle = ( atcNormal, atcUnderscore );
  TWordClass = ( wcNone, wcWord, wcATC, wcMeta );

function CreateFLINKresultLine( const AATC1, AATC2, AName1, AName2, rel, ATextInfo: string ): string;
function GetWordClass( WordToCheck: string ): TWordClass;
function ATCfromNode( NodeText: string ): string;
procedure NormalizeATC( var ATC: string );
function NormalizedATC( const ATC: string ): string;
function UnderscoredATC( const ATC: shortstring ): string;
function GetIntPortion( Source, Tag: string ): integer;
function GetAttr( Source, Tag, Attr: string ): string;
function GetPortion( Source, Tag: string ): string;

const
  XMLResultErr001 = 'XML not retrieved correctly: %s'#10'%s';

  sATCIdx = 'ATCIdx';
  sATC1Idx = 'ATC1Idx';
  sATC2Idx = 'ATC2Idx';
  sATCxIdx = 'ATC%dIdx';
  sSubstIdx = 'SubstIdx';
  sMemberIdx = 'MembIdx';

  sInter = 'INTERACTIONS';
  sSubst = 'SUBSTANCES';
  sMeta = 'METAGROUPS';
  sMemb = 'METAMEMBERS';

  fnATC = 'ATC';
  fnATC1 = 'FraATCKode';
  fnATC2 = 'TilATCKode';
  fnName = 'Name';
  fnSubst = 'Substance';
  fnLang = 'Language';
  fnCheckedOut = 'CheckedOut';
  fnMetaname = 'MetaName';
  fnMetaATC = 'MetaATC';
  fnMemberATC = 'MemberATC';
  fnDesc = 'Informasjon';
  fnRel = 'RelevansId';

  maxRel = 4;

  sSplit = ' - ';
  sListBoxFormat = '%s - %s';

  TAG_LIST  = 'InteractionList';
  TAG_ROW   = 'Interaction';
  TAG_ATC1  = 'ATC1';
  TAG_ATC2  = 'ATC2';
  TAG_NAME1 = 'Name1';
  TAG_NAME2 = 'Name2';
  TAG_REL   = 'Level';
  TAG_TEXT  = 'Text';

implementation

const
  XSTRING01 = 'XSTRING01: XML is not well formed.';
  XSTRING02 = 'XSTRING02: XML Tag (%s) does not exist.';
  XSTRING03 = 'XSTRING03: XML Attribute (%s) does not exist';
  XSTRING04 = 'XSTRING04: Begin quote missing.';
  XSTRING05 = 'XSTRING05: End quote missing.';

function GetAttr(Source, Tag, Attr: string): string;
var
  StartPos, EndPos: integer;
begin
  Result := EmptyStr;
  StartPos := Pos('<' + Tag, Source);
  if StartPos < 1 then raise EConvertError.Create( Format( XSTRING02, [Tag] ));
  Source := Copy(Source, StartPos + Length(Tag) + 1, Length(Source));
  StartPos := Pos(Attr, Source);
  if StartPos < 1 then raise EConvertError.Create( Format( XSTRING03, [Tag] ));
  Source := Copy(Source, StartPos + Length(Attr), Length(Source));
  StartPos := Pos('"', Source);
  if StartPos < 1 then raise EConvertError.Create(XSTRING04);
  Source := Copy(Source, StartPos + 1, Length(Source));
  EndPos := Pos('"', Source);
  if EndPos < 1 then raise EConvertError.Create(XSTRING05);
  Result := Copy(Source, 1, EndPos-1);
end;


function GetPortion(Source, Tag: string): string;
var
  StartPos: integer;
  EndPos: integer;
begin
  Result := EmptyStr;
  Tag := '<' + Tag + '>';
  StartPos := Pos( Tag, Source);
  if StartPos = 0 then exit;
  Result := Copy(Source, StartPos + Length(Tag), Length(Source));
  Insert('/', Tag, 2);
  EndPos := Pos(Tag, Result);
  if EndPos = 0 then
    raise Exception.Create( 'Tag not found: '+Tag );
  Result := Trim(Copy(Result, 1, EndPos - 1));
end;


function GetIntPortion(Source, Tag: string): integer;
begin
  try
    Result := StrToInt(GetPortion(Source, Tag));
  except on Exception do
    Result := 0
  end;
end;

function UnderscoredATC( const ATC: shortstring ): string;
begin
  Result := Trim( ATC );
  if Length(Result)>4 then
    case Result[5] of
      ' ':Result[5] := '_';
      '_':;
    else
      Insert( '_', Result, 5 );
    end;
end;

procedure NormalizeATC(var ATC: string);
begin
  ATC := StringReplace(Trim(ATC), '_', ' ', []);
  if (Length(ATC) > 4) and (ATC[5] <> ' ') then Insert(' ', ATC, 5);
end;


function NormalizedATC( const ATC: string ): string;
var
  s: string;
begin
  s := ATC;
  NormalizeATC( s );
  Result := s;
end;

function GetWordClass(WordToCheck: string): TWordClass;
begin
  case Length(WordToCheck) of
    0: Result := wcNone;
    1:
      if WordToCheck[1] in ['A'..'Z'] then
        Result := wcATC
      else
        Result := wcNone;
    2..8:
      if (WordToCheck[2] in ['0'..'9'])
        and (WordToCheck[1] in ['A'..'Z']) then
        Result := wcATC
      else
        if Length(WordToCheck) < 4 then
          Result := wcNone
        else
          Result := wcWord;
  else
    Result := wcWord;
  end;
  if (Result = wcATC) and (WordToCheck[1] = 'Z') then
    Result := wcMeta;
end;


function ATCfromNode(NodeText: string): string;
var
  dashpos: integer;
begin
  if Length(nodetext) <= 8 then begin
    if nodetext = 'Alle' then
      Result := ''
    else
      Result := nodetext;
    exit;
  end;
  dashpos := Pos(sSplit, NodeText);
  if dashpos <> 0 then
    Result := Copy(NodeText, 1, dashpos - 1)
  else
    Result := '';
end;


function CreateFLINKresultLine( const AATC1, AATC2, AName1, AName2, rel, ATextInfo: string): string;
begin
  Result := '<' + TAG_ROW + '>'+
    Format(
    '<' + TAG_REL + '>%s</' + TAG_REL + '>' +
    '<' + TAG_ATC1 + '>%s</' + TAG_ATC1 + '>' +
    '<' + TAG_NAME1 + '>%s</' + TAG_NAME1 + '>' +
    '<' + TAG_ATC2 + '>%s</' + TAG_ATC2 + '>' +
    '<' + TAG_NAME2 + '>%s</' + TAG_NAME2 + '>' +
    '<' + TAG_TEXT + '>%s</' + TAG_TEXT + '>',
    [rel,AATC1, AName1, AATC2, AName2, ATextInfo]) + '</' + TAG_ROW + '>';
end;

end.


