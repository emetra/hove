program HoveTest;

uses
  MrLogObject,
  Forms,
  HoveMain in 'HoveMain.pas' {frmHoveMain},
  DrugChecker in 'DrugChecker.pas';

{$R *.res}

begin
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  Application.CreateForm(TfrmHoveMain, frmHoveMain);
  Application.Run;
end.
