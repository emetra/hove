unit MrBase;

{
  This module contains interfaces and types that should be available to every
  application from Emetra.  It must be the first to initialize, and the last to
  finalize.  This can be achieved by adding MrLogObject as the first uses clause
  in the DPR file.
}

interface

Uses Classes, Controls, DateUtils, Dialogs, Db, Forms, Graphics, Inifiles, SysUtils, StdCtrls, Windows;

type

  TDebugStrategy = (dbgNone, dbgWriteToLog, dbgShowDialog);
  TOnDialogFunction = function(const Msg: string; DlgType: TMsgDlgType; Buttons: TMsgDlgButtons; HelpCtx: Longint): Integer; {
    Summary:
    A function compatible with the MessageDlg standard function, used for OnDialog property }

  TLogEntryType = (ltDebug, ltInfo, ltMessage, ltWarning, ltError, ltCritical);
  TLogLevel = TLogEntryType;

  TBoolEvent = procedure(Sender: TObject; const AValue: boolean) of object;
  TGetStringFunction = function(const s: string; const ATrim: boolean = false): string;
  TGetValueEvent = procedure(const AVarName: string; var AValue: Variant) of object;
  TNotifyStringEvent = procedure(const s: string) of object;
  TNotifyBoolEvent = procedure(const AValue: boolean) of object;
  TNotifyIntegerEvent = procedure(const AValue: Integer) of object;
  TNotifyProgressEvent = procedure(const AProgress: double) of object;
  TNotifySuccessEvent = function(Sender: TObject): boolean of object;
  TOnPasswordEvent = function(var AUsername, APassword: string): boolean of object;
  TReplaceStringEvent = function(Sender: TObject; const AInput: string): string of object;
  TSelectPersonEvent = function(APersonID: Integer): boolean of object;
  TSearchEvent = function(s: string): TDataset of object;

{$REGION 'General interfaces'}

  IAccessible = interface
    ['{47302C45-B736-470F-9152-423D163C656A}']
    function Instance: TObject;
  end;

  IDisposable = interface
    ['{6BEAF209-8CAB-42D4-B7D4-40412CC144FB}']
    procedure Dispose;
  end;

  IObjectList = interface ['{B02B0D16-3807-49C0-92F6-C84A016CDAED}']
    function Add( AObject: TObject ): integer;
    function Count: integer;
  end;

  IEventMap = interface( IAccessible ) ['{B377BF36-8595-424C-9F90-761BF2CF2EFE}']
    function Get_EventsPerDay: integer;
    property EventsPerDay: Integer read Get_EventsPerDay;
    // function DateToEventNo( const ADateTime: TDateTime ): integer;
    function EventNoToDate( const AEventNo: integer ): TDateTime;
  end;

  IListBoxBase = interface(IAccessible)
    ['{3C17179F-3973-4890-94A5-C51ED952D4E8}']
    function AsListBox(const ASimple: boolean = true): string;
    function IsCurrent: boolean;
  end;

  IListBoxItem = interface( IListBoxBase )
    ['{BFBAEE2B-3BEF-427C-8DD4-E56330192C3C}']
    function CodeText: string;
    function HeaderText: string;
    function MainText: string;
    function Match(const AFilterText: string): boolean;
    function RedText: string;
  end;

  IStatus = interface
    ['{C3F25F6E-92B3-45B5-B26B-16F876EFEF2D}']
    procedure Done;
    procedure SetProgressMessage(const s: string);
    property Info: string write SetProgressMessage;
  end;

  IProgress = interface
    ['{EF5C8790-D9A9-4374-9F24-9981A492A600}']
    procedure Done;
    procedure SetProgress(const APercent: double);
    procedure SetProgressMessage(const s: string);
    procedure SetHeader(const s: string);
    property Header: string write SetHeader;
    property Info: string write SetProgressMessage;
    property Percent: double write SetProgress;
  end;

  ILogger = interface(IInterface)
    ['{C006C5A5-3041-408C-8225-5E852931ECD1}']
    { Accessors }
    function Get_AppPath: string;
    function Get_Lines: TStrings;
    function Get_Threshold: TLogLevel;
    function Get_ThresholdForDialog: TLogLevel;
    procedure Set_Lines(const Value: TStrings);
    procedure Set_Threshold(ALevel: TLogLevel);
    procedure Set_ThresholdForDialog(ALevel: TLogLevel);
    { Other members }
    function GetMarker( const ALevel: TLogEntryType ): string;
    function LogYesNo(const s: string; const ALevel: TLogEntryType = ltMessage; const ACancel: boolean = false): boolean;
    function SetPriority(const APriorityClass: dword): boolean;
    function StripNewlines(const AMsg: string): string;
    procedure CustomDraw(Control: TWinControl; Index: Integer; Rect: TRect; State: TOwnerDrawState);
    procedure CustomDrawColors(ACanvas: TCanvas; const AIndex: Integer);
    procedure Event(const s: string; const AParams: array of const ; const ALogLevel: TLogEntryType = ltInfo); overload;
    procedure Event(s: string; const ALogLevel: TLogEntryType = ltInfo); overload;
    procedure LogString(const AName, AValue: string);
    procedure SaveFatalExit;
    procedure SetFilename(const AFilename: string; const AAppend: boolean = false);
    procedure SetUserFile(AFile: TIniFile);
    procedure ShowMessage(const AMessage: string; const ALevel: TLogEntryType = ltMessage; const AMaxTimes: Integer = maxint);
    property AppPath: string read Get_AppPath;
    property Lines: TStrings read Get_Lines write Set_Lines;
    property Threshold: TLogLevel read Get_Threshold write Set_Threshold;
    property ThresholdForDialog: TLogLevel read Get_ThresholdForDialog write Set_ThresholdForDialog;
  end;

  ILoad = interface(IInterface)
    ['{8E552BB9-C6D5-462F-9C57-7CEE317A73AB}']
    procedure Load(ADataset: TDataset);
  end;

  ISave = interface(IInterface)
    ['{2CAAEBEC-AA9C-4D79-814F-DE417E4E64FE}']
    function Save: boolean;
    function Saved: boolean;
  end;

  { Genders }
  TSex = (sexUnknown, sexMale, sexFemale);
  TSexSet = set of TSex;

  IPersonReadOnly = interface( IAccessible )
    ['{C2458FA3-50A0-411B-BC10-E4B88EF59E42}']
    function Get_DOB: TDateTime;
    function Get_FirstName: string;
    function Get_FullName: string;
    function Get_GenderId: Integer;
    function Get_LastName: string;
    function Get_NationalId: string;
    functioN Get_PersonId: Integer;
    function Get_Sex: TSex;
    function Get_VisualId: string;
    function ShortId: string;
    function Valid: boolean;
    property DOB: TDateTime read Get_DOB;
    property FirstName: string read Get_FirstName;
    property FullName: string read Get_FullName;
    property GenderId: Integer read Get_GenderId;
    property LastName: string read Get_LastName;
    property NationalId: string read Get_NationalId;
    property PersonId: Integer read Get_PersonId;
    property Sex: TSex read Get_Sex;
    property VisualId: string read Get_VisualId;
  end;

  IGeoLocationReadOnly = interface( IAccessible )['{E47D815B-23E9-4721-BAE0-352DA7648E53}']
    function Get_City: string;
    function Get_SiteName: string;
    function Get_PostCode: string;
    function Get_StreetAddress: string;
    function Get_StreetNumber: string;
    property City: string read Get_City;
    property SiteName: string read Get_SiteName;
    property PostCode: string read Get_PostCode;
    property StreetAddress: string read Get_StreetAddress;
    property StreetNumber: string read Get_StreetNumber;
  end;

  IDatabaseUser = interface( IPersonReadOnly )
  ['{338F0CCD-2436-4926-8DC7-DEC86463A85A}']
    function Get_UserId: integer;
    function Get_UserName: string;
    function AddUser(const AUsername, APassword: string): Boolean;
    procedure Clear;
    procedure Populate;
    property UserId: integer read Get_UserId;
    property UserName: string read Get_UserName;
  end;

  IPersonList = interface
    ['{2CCFDBFA-19CB-4BC1-838E-DAA401B15C70}']
    function Count: Integer;
    function Get_Person(AIndex: Integer): IPersonReadOnly;
    function Name: string;
    function Search(const ASearchText: string): Integer;
    procedure Set_Provider(const AName: string);
    property Items[AIndex: Integer]: IPersonReadOnly read Get_Person; default;
    property Provider: string write Set_Provider;
  end;

  IPerson = interface(IPersonReadOnly)
    ['{4668C6B3-9211-427C-9D5F-0E08389688AF}']
    function Instance: TObject;
    function Get_Age: double;
    function Get_PersonId: Integer;
    function Get_SexStr: string;
    procedure Clear;
    procedure SetAddress(const AStreet, APostCode, ACity: string);
    procedure Set_DOB(const AValue: TDateTime);
    procedure Set_FirstName(const AValue: string);
    procedure Set_FullName(const AValue: string);
    procedure Set_GenderId(const AValue: Integer);
    procedure Set_LastName(const AValue: string);
    procedure Set_NationalId(const AValue: string);
    procedure Set_PersonId(const AValue: Integer);
    property Age: double read Get_Age;
    property DOB: TDateTime read Get_DOB write Set_DOB;
    property FirstName: string read Get_FirstName write Set_FirstName;
    property FullName: string read Get_FullName write Set_FullName;
    property GenderId: Integer read Get_GenderId write Set_GenderId;
    property LastName: string read Get_LastName write Set_LastName;
    property NationalId: string read Get_NationalId write Set_NationalId;
    property PersonId: Integer read Get_PersonId write Set_PersonId;
    property SexStr: string read Get_SexStr;
  end;

  TEditMode = (emNone, emAddCase, emEditCase);

  IPersonEditor = interface(IPersonReadOnly)
    ['{BB6593A7-D3E3-4A6B-89CD-6ABA455DBE37}']
    function Add(const AMySelf: boolean): boolean;
    function Edit(const AMySelf: boolean; APerson: IPersonReadOnly): boolean;
  end;

  IDataSession = interface(IAccessible)
    ['{61DB9085-1750-41F5-97B0-F5DCEB140EB6}']
    { Accessors }
    function Get_Id: integer;
    procedure Set_Id(const AValue: integer);
    { Other members }
    property Id: integer read Get_Id write Set_Id;
  end;

  ILoginContext = interface(IAccessible) ['{49A6B91F-3186-4FCF-8B0C-02A00E602E66}']
    { Accessors }
    function Get_ConnectionString: string;
    function Get_UserName: string;
    function Get_Password: string;
    { Other methods }
    property Password: string read Get_Password;
    property UserName: string read Get_UserName;
    property ConnectionString: string read Get_ConnectionString;
  end;

  ISimpleDatabase = interface(ILoginContext)
    ['{2713F75C-2FBE-4551-AE3A-4EBFC829CA2A}']
    { Accessors }
    function Get_OnLogin: TOnPasswordEvent;
    procedure Set_OnLogin(AOnPassword: TOnPasswordEvent);
    { Other methods}
    function Connect( AUser: IDatabaseUser; s: string): boolean;
    function Connected: boolean;
    function Connection: TObject;
    function ExecuteCommand(const ASQL: string): Integer; overload;
    function ExecuteCommand(const ASQL: string; const AParams: array of Variant): Integer; overload;
    function FastQuery(const ASQL: string): TDataset; overload;
    function FastQuery(const ASQL: string; const AParams: array of Variant): TDataset; overload;
    function GetDataset(const ASQL: string): TDataset;
    function PrepareQuery( const ASQL: string; const AParams: array of Variant ): TDataset;
    procedure Disconnect;
    property OnPassword: TOnPasswordEvent read Get_OnLogin write Set_OnLogin;
  end;

  IDatabase = interface( ISimpleDatabase )  ['{C11F8C32-3C7B-46A8-940B-2734B0CC6A04}']
    { Property Accessors }
    function Get_BeforeDisconnect: TNotifyEvent;
    function Get_DbName: string;
    function Get_DbVersion: integer;
    procedure Set_BeforeDisconnect( AValue: TNotifyEvent );
    { Other members }
    function AddUser( AMyUser: IDatabaseUser; const AUserName, APassword: string ): boolean;
    function MakeSelection( const AQuery: string; const AParams: array of variant;
      const AHeader,AText,AEmptyMsg: string; const AutoSelectSingle: boolean = true;
      const AMissingLevel: TLogLevel = ltMessage ): integer;
    function MakeSelectionString( const AQuery: string; const AParams: array of variant;
      const AHeader,AText,AEmptyMsg: string; const AutoSelectSingle: boolean = true;
      const AMissingLevel: TLogLevel = ltMessage ): string;
    function OpenSession( ASession: IDataSession ): boolean;
    procedure BeginTrans( const ATransName: string );
    procedure CloseSession;
    procedure CommitTrans( const ATransName: string );
    procedure ExecuteScript( const AFileName,ATransName: string );
    procedure RollbackTrans( const ATransName: string );
    procedure SetSelectionArea( AControl: TWinControl );
    procedure UpgradeDatabase( const ADesiredVersion: integer );
    property BeforeDisconnect: TNotifyEvent read Get_BeforeDisconnect write Set_BeforeDisconnect;
    property DbVersion: integer read Get_DbVersion;
    property DbName: string read Get_DbName;
  end;

  IRegExEngine = interface(IInterface)
    ['{6DF2C75E-6998-421E-AF53-2F00C5B2190C}']
    { Accessors }
    function GetMatchedExpression: string;
    function GetSubject: string;
    function GetReplacement: string;
    procedure SetReplacement(const AReplacement: string);
    procedure SetRegEx(const ARegEx: string);
    procedure SetSubject(const ASubject: string);
    { Other methods }
    function Match: boolean;
    function MatchAgain: boolean;
    function ReplaceAll: boolean;
    procedure Study;
    property MatchedExpression: string read GetMatchedExpression;
    property RegEx: string write SetRegEx;
    property Replacement: string read GetReplacement write SetReplacement;
    property Subject: string read GetSubject write SetSubject;
  end;

  TSettingScope = (ssGlobal, ssUser, ssMachine, ssMachineUser);

  IParameters = interface
    ['{2D269D74-2DD5-4923-95E6-52D629258AB3}']
    function Flag(const AFlag: string): boolean;
    function Exists(const AKey: string): boolean;
    function ReadBool(const AKey: string; const ADefault: boolean = false): boolean;
    function ReadInteger(const AKey: string; const ADefault: Integer = 0): Integer;
    function ReadFloat(const AKey: string; const ADefault: double = 0): double;
    function ReadString(const AKey: string; const ADefault: string = ''): string;
    function ReadDate(const AKey: string; const ADefault: TDateTime): TDateTime;
  end;

  ISettings = interface(IParameters)
    ['{F94E287C-2EF7-4C20-94B8-7B39AC98EAAB}']
    { Accessors }
    function Get_Context: string;
    procedure Set_Context(const ASettingContext: string);
    function Get_Scope: TSettingScope;
    procedure Set_Scope(const ASettingScope: TSettingScope);
    { Other properties }
    procedure WriteString(const AKey, AValue: string);
    procedure WriteDateTime(const AKey: string; const ADateTime: TDateTime);
    procedure WriteInteger(const AKey: string; const AValue: Integer);
    procedure WriteFloat(const AKey: string; const AValue: double);
    procedure WriteBool(const AKey: string; const AValue: boolean);
    property Context: string read Get_Context write Set_Context;
    property Scope: TSettingScope read Get_Scope write Set_Scope;
  end;

  IAppObjects = interface( IAccessible ) ['{7756A40A-6275-4620-B00B-13DCCAAD55AF}']
    function Get_Item( AIndex: integer ): TObject;
    function Count: integer;
    function Find( const AName: string; out AObject: TObject ): boolean;
    procedure AddAlias( const AName: string; AObject: TObject );
    procedure Adopt( const AName: string; AObject: TObject ); overload;
    procedure Adopt( AObject: TObject ); overload;
    property Items[AIndex: integer ]: TObject read Get_Item; default;
  end;

{$ENDREGION}
{$REGION 'Design pattern interfaces'}
  { Observer design pattern }
  IObserver = interface(IAccessible)
    ['{1E0CFE05-0FC6-4145-9FB9-4F92433BA1FB}']
    procedure AfterUpdate(Sender: TObject);
  end;

  ISubject = interface(IInterface)
    ['{B8138904-D1A3-4B65-A40E-09FC37BF4BDC}']
    procedure Attach(AObserver: IObserver);
    procedure Detach(AObserver: IObserver);
    procedure BeginUpdate;
    procedure EndUpdate;
  end;

  IDatabaseObserver = interface(IObserver)
    ['{47CF1A13-91C9-414F-868B-3630FB2586A2}']
    procedure AfterLogin(Sender: TObject);
    function FriendlyName: string;
  end;

{$ENDREGION}
  IStringSelector = interface
    ['{205522FB-DA12-47E9-B064-ED171D3931D9}']
    function SelectString(const AHeading, ADetail: string; AItems: TStrings; ACoverThis: TWinControl = nil): string;
    function SelectColorful(const AHeader, ADetail: String; AItems: TStrings; ACoverThis: TWinControl = nil): string;
  end;

var
  LOG: ILogger = nil;
  RGX: IRegExEngine = nil;
  StrSelect: IStringSelector = nil;
  Progress: IProgress = nil;
  Status: IStatus = nil;
  PersonEditor: IPersonEditor = nil;
  AppObjects: IAppObjects = nil;
  EventMap: IEventMap = nil;

resourcestring
  LOG_IGNORE = 'Klikk "%s" hvis du ikke vil se denne meldingen heretter.';
  BTN_IGNORE = 'Skjul';
  EXC_DIRTY_INTERFACES = 'MrBase.pas: Minst ett interface er ikke satt til nil.';

const
  LOG_ENTRY = 'Called';
  LOG_SUCCESS = 'Success!';
  ltTrivialInfo = ltDebug;
  ltException = ltError;
  TAB = #9;
  CR = #13;
  LF = #10;
  CRLF = CR + LF;
  SINGLE_QUOTE = '''';
  QUOTECHARS = ['"', SINGLE_QUOTE];
  TAG_FONT_COLOR = ' ^';
  TAG_YELLOW = TAG_FONT_COLOR + 'y';
  TAG_RED = TAG_FONT_COLOR + 'r';
  TAG_GREEN = TAG_FONT_COLOR + 'g';
  TAG_BLUE = TAG_FONT_COLOR + 'b';
  TAG_OLIVE = TAG_FONT_COLOR + 'o';
  TAG_PURPLE = TAG_FONT_COLOR + 'l';

  ONE_SECOND = 1 / 24 / 60 / 60;
  LOG_NEWLINE = '\n';
  LOG_COLON = ':' + LOG_NEWLINE;
  LOG_CREATE = '%s.Create';
  LOG_DESTROY = '%s.Destroy';

  RTF_START = '{\rtf1';
  RTF_TEMPLATE = RTF_START + '\ansi %s}';

function GetMessageDigest(const s: string): Integer;
function GetWindowsUserName: string; { Returns the user currently logged in to OS }
function GetWindowsComputerName: string; { Returns the computer this application runs on }
function GetWindowsDomainName: string;

function GetTempDir: string;
function BoolToInt( const ABoolean: boolean ): integer;
procedure NilCheck(const AVarName: string; Sender: TObject); overload;
procedure NilCheck(const AVarName: string; Sender: IInterface); overload;
procedure SetStartupDir;
procedure ClearBaseInterfaces;

var
  ExeParams: TStringList; { Parameters on startup.  First is ExeName }

implementation

function GetMessageDigest(const s: string): Integer;
var
  Off, Len, Skip, I: Integer;
begin
  Result := 0;
  Off := 1;
  Len := Length(s);
  if Len < 16 then
    for I := (Len) downto 1 do begin
      Result := (Result * 37) + Ord(s[Off]);
      Inc(Off);
    end
    else begin
      { Only sample some characters }
      Skip := Len div 4;
      I := Len;
      while I >= 1 do begin
        Result := (Result * 39) + Ord(s[Off]);
        Dec(I, Skip);
        Inc(Off, Skip);
      end;
    end;
end;
{$REGION 'WindowsNames'}

function GetWindowsUserName: string;
const
  cnMaxLen = 254;
var
{$IFDEF DotNet}
  sUserName: stringbuilder;
{$ELSE}
  sUserName: array [0 .. cnMaxLen] of char;
{$ENDIF}
  dwUserNameLen: dword;
begin
  dwUserNameLen := cnMaxLen - 1;
  GetUserName(sUserName, dwUserNameLen);
{$IFDEF DotNet}
  Result := sUserName.ToString;
{$ELSE}
  Result := strPas(sUserName);
{$ENDIF}
end;

function GetWindowsComputerName: string;
const
  cnMaxLen = 254;
var
{$IFDEF DotNet}
  sUserName: stringbuilder;
{$ELSE}
  sUserName: array [0 .. cnMaxLen] of char;
{$ENDIF}
  dwUserNameLen: dword;
begin
  dwUserNameLen := cnMaxLen - 1;
  GetComputerName(sUserName, dwUserNameLen);
{$IFDEF DotNet}
  Result := sUserName.ToString;
{$ELSE}
  Result := strPas(sUserName);
{$ENDIF}
end;

function GetTempDir: string;
var
{$IFDEF DotNet}
  pCurrTempDir: stringbuilder;
{$ELSE}
  pCurrTempDir: array [0 .. 255] of char;
{$ENDIF}
begin
  Windows.GetEnvironmentVariable('TEMP', pCurrTempDir, 255);
{$IFDEF DotNet}
  Result := IncludeTrailingPathDelimiter(pCurrTempDir.ToString);
{$ELSE}
  Result := IncludeTrailingPathDelimiter(strPas(pCurrTempDir));
{$ENDIF}
end;

function GetWindowsDomainName: string;
var
  pUserDomain: array [0 .. 255] of char;
begin
  Windows.GetEnvironmentVariable('USERDOMAIN', pUserDomain, 255);
  Result := strPas(pUserDomain);
end;
{$ENDREGION}
{$REGION 'NilCheck'}

procedure NilCheck(const AVarName: string; Sender: TObject);
begin
  if not Assigned(Sender) then
    raise EInvalidPointer.CreateFmt('%s is nil', [AVarName]);
end;

procedure NilCheck(const AVarName: string; Sender: IInterface);
begin
  if not Assigned(Sender) then
    raise EInvalidPointer.CreateFmt('%s is nil', [AVarName]);
end;
{$ENDREGION}

procedure SetStartupDir;
var
  strStartDir: string;
begin
  strStartDir := ExeParams.Values['DIR'];
  if (strStartDir <> EmptyStr) and DirectoryExists(strStartDir) then
    SetCurrentDir(strStartDir)
  else
    SetCurrentDir(ExtractFilePath(Application.ExeName));
end;

procedure ClearBaseInterfaces;
begin
  Status := nil;
  Progress := nil;
  StrSelect := nil;
  RGX := nil;
end;

function BoolToInt( const ABoolean: boolean ): integer;
begin
  if ABoolean = true then
    Result := 1
  else
    Result := 0;
end;

procedure CheckExitState;
begin
  { This should be the very last unit that is finalized}
  try
  if Assigned(Status) or Assigned(Progress) or Assigned(StrSelect) or Assigned( RGX )
  or Assigned( PersonEditor ) or Assigned( EventMap ) then
    raise EAssertionFailed.Create( EXC_DIRTY_INTERFACES );
  finally
    ClearBaseInterfaces;
    LOG := nil;
  end;
end;

var
  n: Integer;

{ This unit must the the first to initialize and the last to finalize.
  This is tested with asserting that LOG is nil on startup.
  Use F7 to step-debug for verification. }

initialization
  Assert( LOG = nil );
  ExeParams := TStringList.Create;
  for n := 0 to ParamCount do
    ExeParams.Add(ParamStr(n));
finalization
  CheckExitState;
  ExeParams.Free;
end.
